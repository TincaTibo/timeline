NAME            := timeline
NPM_REGISTRY    := $(shell npm config get registry)
NPM             := npm

node_modules: package.json ## install node modules
	$(NPM) install --quiet --registry=$(NPM_REGISTRY)

clean: node_modules ## remove node_modules directory
	$(NPM) run clean

build: node_modules ## build dist
	$(NPM) run build && printf "\nRemember to:\n  - Update \033[36mCHANGELOG.md\033[0m\n  - Increase version: \033[36mnpm version major|minor|patch\033[0m\n"

publish: build ## publish to npm
	$(NPM) publish --access public && git push origin --tags

help: ## display help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z._-]+:.*?## / {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help
