# Build bundle

FROM node:16-alpine AS build_bundle

WORKDIR /app
COPY package*.json /app/

RUN apk --no-cache add python3 build-base

ARG registry
RUN npm install --quiet --no-progress --registry=${registry:-https://registry.npmjs.org}

COPY . /app
RUN npm run build_gui \
    && npm run build_server \
    && npm cache clean --force \
    && npm prune --production

# Build delivery

FROM node:16-alpine AS build

WORKDIR /app
COPY package.json docker-entrypoint.sh /app/
COPY --from=build_bundle /app/node_modules /app/node_modules
COPY --from=build_bundle /app/dist /app/
COPY --from=build_bundle /app/bundle /app/bundle

# Final

FROM node:16-alpine

WORKDIR /app
COPY --from=build /app /app/

ARG port
EXPOSE $port
USER 1000:1000
ENTRYPOINT ["./docker-entrypoint.sh"]
CMD ["index.js"]
