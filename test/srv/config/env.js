module.exports = {
    applicationName: process.env.APPLICATION_NAME || 'Timeline',
};

process.title = `${module.exports.applicationName}.js`;
