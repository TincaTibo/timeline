"use strict";

const koa = require('koa');
const cors = require('@koa/cors');
const compress = require('koa-compress');
const router = require('@koa/router')();
const serve = require('koa-static');

exports.initRoutes = async function({serverPort, applicationName}){
    const app = new koa();

    app.name = applicationName;

    app.use(cors());
    app.use(compress());

    if(process.env.NODE_ENV === 'development'){
        app.use(require('@koa/cors')());

        const webpack = require('webpack');
        const webpackDevMiddleware = require('webpack-dev-middleware');
        const webpackHotMiddleware = require('webpack-hot-middleware');
        const { koaDevMiddleware, koaHotMiddleware } = require('./koa-webpack-middleware');

        const webpackConfig = require('../../webpack.config.gui');

        const compiler = webpack(webpackConfig)
        // dev
        const expressDevMiddleware = webpackDevMiddleware(compiler, {
            /* opt */
            publicPath: '/',
            stats: {
                colors: true
            }
        })
        app.use(koaDevMiddleware(expressDevMiddleware))

        // hot
        const expressHotMiddleware = webpackHotMiddleware(compiler, {
            path: `/__webpack_hmr`,
        })
        app.use(koaHotMiddleware(expressHotMiddleware));
        app.use(router.routes());

    }
    else {
        app.use(require('koa-compress')());
        app.use(require('koa-conditional-get')());
        app.use(require('koa-etag')());
        app.use(require('koa-mount')('/', serve('bundle', { maxage: 10*60*100 })));
    }

    app.use(require('koa-mount')('/plugins', serve('plugins')));
    app.use(serve('public'));

    app.use(router.routes());

    return new Promise(function (resolve, reject) {
        const server = app.listen(serverPort, (err) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(server); //returns the server
            }
        });
    });
};

