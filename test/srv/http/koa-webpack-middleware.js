function koaDevMiddleware(expressDevMiddleware) {
    return function middleware(ctx, next) {
        return new Promise((resolve) => {
            expressDevMiddleware(ctx.req, {
                end: (content) => {
                    ctx.body = content;
                    resolve(false);
                },
                setHeader: (name, value) => {
                    ctx.set(name, value);
                },
                getHeader: (name, value) => {
                    ctx.get(name, value);
                },
            }, () => {
                resolve(true);
            });
        }).then((err) => {
            if (err) { return next(); }
            return null;
        });
    };
}

const PassThrough = require('stream').PassThrough

function koaHotMiddleware(expressHotMiddleware) {
    return async (ctx, next) => {

        if (ctx.request.path !== '/__webpack_hmr') {
            return await next();
        }

        let stream = new PassThrough();
        ctx.body = stream;

        return await expressHotMiddleware(ctx.req, {
            write: stream.write.bind(stream),
            writeHead: (status, headers) => {
                ctx.status = status;
                Object.keys(headers).forEach(key => {
                    ctx.set(key, headers[key]);
                });
            },
            end: () => {
                stream.end();
            }
        }, next);
    }
}

module.exports = {
    koaHotMiddleware,
    koaDevMiddleware
}
