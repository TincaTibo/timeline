import React, { Component } from 'react';
import moment from 'moment';
import TimeLine from '../timeline';

import ReactNotifications, {store} from "react-notifications-component";

import "react-notifications-component/dist/theme.css";

import {formatNumber, multiFormat} from './locale';
import withStyles from '@material-ui/styles/withStyles';

const styles = {
    timeline: {
        width: '100%',
        height: '100%',
        backgroundColor: 'white',
    },
    cursorArea: {
        fill: '#4347fdff',
        stroke: '#4347fdff',
        fillOpacity: 0.1,
    },
    cursorLeftHandle: {
        stroke: '#4347fdff'
    },
    cursorRightHandle: {
        stroke: '#4347fdff'
    },
    zoomIn : {
        fill: '#4347fdff'
    },
    legend: {
        transform: 'translateX(-25px)'
    },
    verticalAxisText: {
        fill: '#949494'
    },
    timeAxisText: {
        transform: 'translateY(5px)'
    },
    toolTip: {
        fill: '#ffffff',
        fillOpacity: 0.80,
        stroke: '#000000',
        strokeOpacity: 1,
        strokeWidth: 0.2,
        strokeMiterlimit: 1.3,
    },
    toolTipText: {
        fill: '#676767',
    },
    action: { //local for demo
        color: '#3d7bbf',
        textDecoration: 'underline',
        cursor: 'pointer'
    }
};

export const TIME_FORMAT_TOOLTIP = 'YYYY-MM-DD HH:mm:ss';

const Colors = {
    serverError: '#ffbabbff',
    serverErrorStroke: '#ff6a6b',
    clientError: '#ffffbaff',
    clientErrorStroke: '#cdcd94',
    success: '#b2fb9dff',
    successStroke: '#95cd87',
    info: '#7ee4f5',
    infoStroke: '#6ab1c8',
};

const metricsDefinition = {
    count: 4, //Count of metric in the graphic
    legends: ['Info', 'Success', 'Warn', 'Fail'], //Name of the metrics, in order
    colors: [{ //Colors of the metrics, in order: fill of bar, stroke of bar, text in legend
        fill: Colors.info,
        stroke: Colors.infoStroke,
        text: Colors.infoStroke
    },{
        fill: Colors.success,
        stroke: Colors.successStroke,
        text: Colors.successStroke
    },
    {
        fill: Colors.clientError,
        stroke: Colors.clientErrorStroke,
        text: Colors.clientErrorStroke
    },{
        fill: Colors.serverError,
        stroke: Colors.serverErrorStroke,
        text: Colors.serverErrorStroke
    }]
};

//Should call an API, or default settings... depends on spec
const getDomainAPI = () => ({
    min: moment().subtract(1, 'WEEK').startOf('DAY'),
    max: moment().endOf('DAY')
});

class App extends Component {

    constructor(props){
        super(props);

        this.notificationDOMRef = React.createRef();
        this.timeLine = React.createRef();

        this.state = {
            timeSpan: {
                start: moment().subtract(1, 'HOUR'),
                stop: moment().add(1, 'HOUR'),
            },
            items: [],
            intervalMs: null,
            domains: [], //should be called on index.js, or may be called in the TimeLine when mounting
            maxDomain: {
                min: moment().subtract(2, 'MONTHS').startOf('DAY'),
                max: moment().add(1, 'WEEK').endOf('DAY')
            },
            quality: []
        };
    }

    render(){
        const {classes} = this.props;

        return (
            <div style={{
                width: '100%',
                height: '100%',
            }}>
                <a
                    className={classes.action}
                    onClick={()=>this.timeLine.current.zoomIn()}
                >
                    ZoomIn
                </a>
                &nbsp;
                <a
                    className={classes.action}
                    onClick={()=>this.timeLine.current.zoomOut()}
                >
                    ZoomOut
                </a>
                &nbsp;
                <a
                    className={classes.action}
                    onClick={()=>this.timeLine.current.shiftTimeLine(50)}
                >
                    SlideBackward
                </a>
                &nbsp;
                <a
                    className={classes.action}
                    onClick={()=>this.timeLine.current.shiftTimeLine(-50)}
                >
                    SlideForward
                </a>
                <ReactNotifications ref={this.notificationDOMRef}/>
                <TimeLine
                    ref={this.timeLine}
                    className={classes.timeline}
                    classes={classes}
                    timeSpan = {this.state.timeSpan}                    //start and stop of selection
                    histo = {{
                        items: this.state.items,                        //table of histo columns: [{time: moment, metrics: [metric1 value, metric2 value...], total: sum of metrics}, ...]
                        intervalMs: this.state.intervalMs               //interval duration of column in ms
                    }}
                    showHistoToolTip
                    quality = {{
                        items: this.state.quality,                      //table of quality indicators [{time: moment, quality: float [0,1], tip: string}]
                        intervalMin: this.state.intervalMin             //interval duration of column in Min
                    }}
                    zoomOutFactor = {2}
                    domains = {this.state.domains}                      //array of zoom levels
                    maxDomain = {this.state.maxDomain}                  //max zoom level allowed
                    metricsDefinition = {metricsDefinition}
                    biggestVisibleDomain = {moment.duration('P1M')} //maximum visible duration, cannot zoom out further
                    biggestTimeSpan = {moment.duration('P7D')}      //maximum duration that can be selected
                    smallestResolution = {moment.duration('PT1M')}  //max zoom level: 15pixels = duration
                    labels={{
                        backwardButtonTips: {
                            slideBackward: 'Slide into the past',
                        }
                    }}
                    tools={{
                        slideForward: true,
                        slideBackward: true,
                        resetTimeline: true,
                        gotoNow: true,
                        cursor: true,
                        zoomIn: true,
                        zoomOut: true
                    }}
                    fetchWhileSliding
                    selectBarOnClick

                    xAxis = {{
                        spaceBetweenTicks: 70,
                        barsBetweenTicks: 5,
                        height: 30,
                    }}

                    yAxis = {{
                        spaceBetweenTicks: 28,
                        showGrid: true,
                    }}

                    margin = {{
                        top: 20,
                        bottom: 20,
                        left: 80
                    }}

                    onLoadDefaultDomain = {this.onLoadDefaultDomain}    //called on mount to get the default domain
                    onLoadHisto = {this.onLoadHisto}                    //called to load items. give the needed interval, computed from props.width, props.domains[0]
                    onCustomRange = {this.onCustomRange}                //called when user has drawn or resized the cursor
                    onShowMessage = {this.addNotification}                       //called to display an error message
                    onUpdateDomains = {this.onUpdateDomains}            //called to save domains
                    onResetTime = {this.onResetTime}                    //called when user want to reset timeline
                    onFormatTimeToolTips = {this.onFormatTimeToolTips}  //called to display time in tooltips
                    onFormatTimeLegend = {multiFormat}                  //called to format x axis legend
                    onFormatMetricLegend = {formatNumber}               //called to format y axis metric legend
                />
            </div>
        )
    }

    addNotification = (message) => {
        store.addNotification({
            title: "Timeline",
            message,
            type: "default",
            insert: "top",
            container: "bottom-center",
            animationIn: ["animated", "fadeIn"],
            animationOut: ["animated", "fadeOut"],
            slidingEnter: { duration: 400 },
            dismiss: { duration: 2000 },
            dismissable: { click: true }
        });
    };

    onFormatTimeToolTips = (time) => {
        return moment(time).second(0).millisecond(0).format(TIME_FORMAT_TOOLTIP);
    };

    onLoadDefaultDomain = () => {
        /*
            To set min/max
            - May call an API on service side
            - Or may set to default settings functionally define

            The max domain avoid the ability of the user to zoom out to much and generate long aggregation queries
            However, user can still go back in time by shifting timeline when dragging or with action button
            If it is wanted to block the user after a certain time (like couple of Months... to be determined by the amount of data),
            then you have to block it with maxDomain prop
         */
        console.log('Calling Domain API');
        //simulate network
        setTimeout(function(){
            this.setState({
                domains: [getDomainAPI()]
            });
        }.bind(this), 50);
    };

    onLoadHisto = (intervalMs, start, end) => {
        /*
        Ask for data

        POST _search on using querystring set in state + intervalMs + start + end
            IMHO, start and end should be part of the parameters
                - not to add them to querystring, service adds in query DSL
                - in order to be able to do safety checks on service side. Ex: limit {(end - start)/intervalMs} value
            querystring: ...
            aggs: {
                items: {
                    date_histogram: {
                        field: {{timeField}},
                        interval: {{intervalMs}} ? `${Math.round(intervalMs / 1e3)}s` : '1h' //safety belt
                    },
                    aggs: {
                        eventName: { //example that will give count of each distinct eventName
                            terms: {field: 'eventName'}
                        }
                    }
                }
            }

        - API result example:
            aggs: {
                items: {
                    buckets: [
                        {
                            key_as_string: '2019-02-23T12:12:12.456z',
                            key: 234567898765,
                            eventName: {
                                buckets: [
                                    {
                                        key: WARN_BLABLA,
                                        doc_count: 32
                                    },
                                    {
                                        key: FAIL_BLABLA,
                                        doc_count: 32
                                    }
                                    ...
                                ]
                            }
                        },
                        ...
                    ]
                }
            }

        - API result must be translated into items prop:
            const bucketMetrics: (b) => {
                const res = [0, 0, 0];
                b.eventName.buckets.forEach(state => {
                    if(state.key.startsWith('WARN')){
                        res[1] += state.doc_count;
                    }
                    else if(state.key.startsWith('FAIL')){
                        res[2] += state.doc_count;
                    }
                    else {
                        res[0] += state.doc_count;
                    }
                });
                return res;
            }

            items: res.aggs && res.aggs.items.buckets.map(b => ({
                time: b.key_as_string ? moment(b.key_as_string) : moment.unix(b.key),
                total: _.sum(bucketMetrics(b)), //not optimal
                metrics: bucketMetrics(b)
            })),
         */

        //Here is a fake generating nice cosinus based output
        const items = [];
        const now = moment();

        for(let then = moment(start); then.isBefore(end) && then.isBefore(now); then.add(intervalMs)){
            const metrics = [];
            let total = 0;
            for(let i=0; i<metricsDefinition.count; i++){
                const metric = Math.abs(Math.cos(((+then)/(36*60*60*1000))+i/2)+i/2)*100;
                metrics.unshift(metric);
                total += metric;
            }
            const item = {
                time: moment(then),
                metrics,
                total
            };
            items.push(item);
        }

        const qualityItems = [];
        const intervalMin = intervalMs > 60000 ? Math.floor(intervalMs/60000) : 1;
        const startMin = moment(start).millisecond(0).second(0);
        const endMin = moment(end).millisecond(0).second(0).add(1, 'min');
        for(let then = moment(startMin); then.isBefore(endMin) && then.isBefore(now); then.add(intervalMin, 'minute')){
            const quality = Math.min(Math.random()*3, 1);

            if(quality<1){
                const item = {
                    time: moment(then),
                    quality,
                    tip: <>Quality:<br/>{Math.round(quality*100)}%</>
                };
                qualityItems.push(item);
            }
        }

        console.log('Calling Histo API');

        //Simulate network
        setTimeout(this.setState.bind(this), 50, {
            intervalMs,
            items,
            quality: qualityItems,
            intervalMin
        });
    };

    onCustomRange = (start, stop) => {
        //Redraw cursor

        //Round to minute
        start = moment(start).millisecond(0).second(0);
        stop = moment(stop).millisecond(0).second(0);

        this.setState({
            timeSpan: {
                start,
                stop,
            }
        });

        console.log(`${start.toISOString()} -> ${stop.toISOString()}`);
    };

    onUpdateDomains = (domains) => {
        //Update zoom levels (managed by timeline)
        this.setState({
            domains
        });
    };

    onResetTime = () => {
        //Reset any zoom level - you may or may not reset maxDomain
        console.log('Calling Domain API');

        //simulate network
        setTimeout(function(){
            this.setState({
                domains: [getDomainAPI()]
            });
        }.bind(this), 50);
    };
}

export default withStyles(styles)(App);
