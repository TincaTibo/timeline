import { formatLocale } from 'd3-format';
import moment from "moment-timezone";
import _padStart from 'lodash-es/padStart';

const formatMillisecond = 'SSS';   // 456
const formatSecond = ':ss';        // :43
const formatMinute = 'HH:mm';      // 13:12
const formatHour = 'HH:00';        // 13:00
const formatDay = 'MMM DD';        // Nov 02
const formatMonth = 'MMM DD';      // Nov 01
const formatYear = 'YYYY MMM DD';  // 2024 Nov 01

export function multiFormat(date) {
    const mDate = moment(date);

    // Determine format based on the specificity of the date required
    if (mDate.milliseconds() > 0) {
        return mDate.format(formatMillisecond);
    } else if (mDate.seconds() > 0) {
        return mDate.format(formatSecond);
    } else if (mDate.minutes() > 0) {
        return mDate.format(formatMinute);
    } else if (mDate.hours() > 0) {
        return mDate.format(formatHour);
    } else if (mDate.date() > 1) {
        return mDate.format(formatDay);
    } else if (mDate.month() > 0) {
        return mDate.format(formatMonth);
    } else {
        return mDate.format(formatYear);
    }
}

const locale = formatLocale({
    decimal: '.',
    thousands: ' ',
    grouping: [3],
    currency: ['','€']
});

export const formatNumber = (number, pad, plus) => {
    const formatted = locale.format(plus?`+,d`:`,d`)(number);
    if(!pad){
        return formatted;
    }
    else{
        return _padStart(formatted, pad);
    }
};