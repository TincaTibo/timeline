NAME         := timeline
NPM_REGISTRY := $(shell npm config get registry)
DOCKER_REGISTRY ?= registry.gitlab.com/tincatibo/
PORT         := 5000
NPM          := npm
DOCKER       := docker
GIT          := git
TAG          := latest
IMAGE        := $(DOCKER_REGISTRY)$(NAME):$(TAG)
NOCACHE      ?= 0

ifeq ($(NOCACHE), 1)
		DOCKER_BUILD_OPTS=--no-cache
endif

copy:
	mkdir -p timeline && cp -rf ../index.js ../package.json ../src timeline

node_modules: package.json ## install node modules
	$(NPM) install --quiet --registry=$(NPM_REGISTRY)

image: copy ## build docker image
	$(DOCKER) build $(DOCKER_BUILD_OPTS) --tag $(IMAGE) --build-arg registry=$(NPM_REGISTRY) --build-arg port=$(PORT) .

start: image ## start dev
	$(DOCKER) run -d -p "5000:5000" -e NODE_ENV=development -v $(PWD):/app -v $(PWD)/../src:/app/timeline/src --rm --name TimeLine $(IMAGE) index.js

demo: image ## start demo
	$(DOCKER) run -d -p "5000:5000" --rm --name TimeLine $(IMAGE)

analyse: image ## analyse bundle
	webpack --profile --json --mode=production > stats.json && webpack-bundle-analyzer stats.json

push: ## docker push
	$(DOCKER) push $(IMAGE)

help: ## display help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z._-]+:.*?## / {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help
