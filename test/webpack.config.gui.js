const path = require('path');
const webpack = require('webpack');
const CompressionPlugin = require('compression-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin');

const src = path.resolve(__dirname, 'src');
const timeline = path.resolve(__dirname, 'timeline');
const isDevelopment = process.env.NODE_ENV === 'development';

module.exports = {
    entry: {
        main: isDevelopment ? [
            'webpack-hot-middleware/client?path=./__webpack_hmr&dynamicPublicPath=true&reload=true',
            './src/index.jsx'
        ] : './src/index.jsx'
    },
    output: {
        path: path.join(__dirname, 'bundle'),
        filename: '[name].js',
        publicPath: './'
    },
    target: 'browserslist',
    mode: isDevelopment ? 'development' : 'production',
    devtool: false,
    resolve: {
        extensions: ['.js', '.jsx'],
        alias: {
            src
        },
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                include: [
                    src,
                    timeline
                ],
                use: {
                    loader: "babel-loader",
                    options: {
                        exclude: [
                            /node_modules/,
                        ],
                        presets: [
                            "@babel/preset-react"
                        ],
                        plugins: [
                            require.resolve('babel-plugin-lodash'),
                            require.resolve('@babel/plugin-proposal-export-default-from'),
                            require.resolve('@babel/plugin-proposal-class-properties'),
                            isDevelopment && require.resolve('react-refresh/babel')
                        ].filter(Boolean),
                    },
                }
            },
            {
                test: /\.less|\.css$/,
                use: [{
                    loader: 'style-loader'
                },{
                    loader: 'css-loader'
                },{
                    loader: 'less-loader'
                }]
            }
        ]
    },
    optimization: {
        splitChunks: {
            chunks: 'all'
        }
    },
    plugins: [
        isDevelopment && new webpack.EvalSourceMapDevToolPlugin({
            exclude: [
                /node_modules/,
            ],
        }),
        new HtmlWebpackPlugin({
            template: './src/index.html',
            name: 'index.html',
            inject: 'body',
            publicPath: './'
        }),
        // Ignore all locale files of moment.js - 175K - https://github.com/jmblog/how-to-optimize-momentjs-with-webpack
        new webpack.IgnorePlugin({
            resourceRegExp: /^\.\/locale$/,
            contextRegExp: /moment$/
        }),
        new CompressionPlugin(),
        isDevelopment && new webpack.HotModuleReplacementPlugin(),
        isDevelopment && new ReactRefreshWebpackPlugin({
            overlay: {
                sockIntegration: 'whm',
            },
        }),
    ].filter(Boolean)
};
