"use strict";

const env = require('./srv/config/env');
const Routes = require('./srv/http/routes');

const app = async () => {
    const server = await Routes.initRoutes({serverPort: 5000, applicationName: env.applicationName});

    console.log(`${env.applicationName} listening on port ${server.address().port}!`);

    process.on('SIGTERM', () => {
        server.close(() => {
            setTimeout(() => {
                console.log(`${env.applicationName} exited!`);
                process.exit(0);
            }, 1000)
        });
    });
};

app().catch(err => {
    console.error(err, `Couldn't launch application ${env.applicationName}`);
    process.exit(1);
});

