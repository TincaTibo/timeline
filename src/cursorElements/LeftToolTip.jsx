import React, {useRef, useEffect} from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/styles';

import clsx from 'clsx';
import {hide, show, resizeToolTip, resizeToolTipText} from './utils';
import {toolTipStyles} from './commonStyles';

const useStyles = makeStyles({
    ...toolTipStyles,
});

export default function LeftToolTip({
                                       classes, height, origin,
                                       isVisibleTooltipLeft,
                                       startText,
                                   }) {
    const defaultClasses = useStyles();
    const className = (className) => clsx(defaultClasses[className], classes[className]);

    const toolTipLeft = useRef();

    useEffect(() => {
        if(isVisibleTooltipLeft)
            show(toolTipLeft.current)
        else hide(toolTipLeft.current);
    }, [isVisibleTooltipLeft]);

    return (
        <g transform={`translate(${origin.x}, ${origin.y})`}
           className={className('toolTipArea')}
           ref={toolTipLeft}>
            <g
                transform={`scale(${resizeToolTip(height)})`}
            >
                <rect
                    className={className('toolTip')}
                    x={-2.5}
                    y={-8}
                    width={startText.length*2}
                    height={6.5}
                    rx={1}
                />
                <path
                    className={className('toolTip')}
                    d={'M -1,-1.7 0,-0.5 1,-1.7'}
                />
            </g>
            <text
                className={className('toolTipText')}
                style={{
                    fontSize: resizeToolTipText(height) + 'px'
                }}
                x="-2"
                y={-resizeToolTipText(height)}
            >
                {startText}
            </text>
        </g>
    );
};

LeftToolTip.propTypes = {
    classes: PropTypes.object.isRequired,
    origin: PropTypes.shape({
        x: PropTypes.number.isRequired,
        y: PropTypes.number.isRequired
    }).isRequired,
    height: PropTypes.number.isRequired,
    isVisibleTooltipLeft: PropTypes.bool.isRequired,
    startText: PropTypes.string.isRequired,
}
