import {pointer} from 'd3-selection';

export function handleHistoOver({
   showHistoToolTip, dragging, event, dragOverlay, marginBottom,
   items, height, tooltipVisible, timer, barHovered,
   setTimer, setToolTipVisible, setBarHovered
}) {
    if (showHistoToolTip && !dragging && dragOverlay) {
        const [x, y] = pointer(event, dragOverlay.current);

        //Compute the hovered bar
        const margin = marginBottom || 0;
        const index = items.findIndex(element =>
            (element.x1 <= x)
            && (element.x2 > x)
            && (y >= height - element.height - margin)
            && (y < height - margin)
        );

        //no tooltip displayed and no timer => set to display tooltip in some time
        if (!tooltipVisible && !timer) {
            setTimer(setTimeout(() => {
                setToolTipVisible(true);
                setTimer(null)
            }, 1000));
        }
        //timer but changed bar => reset timer to display tooltip
        else if (timer && barHovered !== index) {
            clearTimeout(timer);

            setTimer(setTimeout(() => {
                setToolTipVisible(true);
                setTimer(null)
            }, 1000));
        }
        //not hover a bar, hide tooltip (and then expect a timer later)
        else if (index < 0 && tooltipVisible) {
            setToolTipVisible(false);
        }

        if(barHovered !== index){
            setBarHovered(index);
        }
    }
}

export function handleHistoOut({setToolTipVisible, setBarHovered, timer, friendTarget}) {
    return event => {
        if(event.relatedTarget !== (friendTarget && friendTarget.current)){
            setToolTipVisible(false);
            setBarHovered(-1);
        }
        if(timer){
            clearTimeout(timer);
        }
    };
}
