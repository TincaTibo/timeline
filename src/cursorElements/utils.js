import _clamp from 'lodash-es/clamp';

export function hide(element) {
    element.style.opacity = 0;
    element.style.visibility = 'hidden';
}

export function show(element) {
    element.style.opacity = 1;
    element.style.visibility = 'visible';
}

export const resizeToolTip = (height) => _clamp(2 * height/30, 1.9, 4);
export const resizeToolTipText = (height) => _clamp(7 * height/29, 7, 14);
export const resizeToolTipRightDelta = (height) => _clamp(4 * height/30,4,9)
