import React, {useState} from 'react';
import ToolTip from '../ToolTip';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/styles';

import theme from '../theme';
import clsx from 'clsx';

const useStyles = makeStyles({
    cursorIconRect: {
        fill: '#00bcd4',
        fillOpacity: 0.25,
        stroke: '#00bcd4',
        strokeWidth: 0.42,
    },
    cursorIconCircle:{
        fill: '#ffffff',
        stroke: '#00bcd4',
        strokeWidth: 0.52,
    },
    cursorIconRectOver: {
        fill: theme.colors.itemFillSelected,
        stroke: theme.colors.itemStrokeSelected,
    },
    cursorIconCircleOver:{
        stroke: theme.colors.itemStrokeSelected,
    }
});

export default function CursorIcon({    classes, rcToolTipPrefixCls, origin,
                                        position, gotoCursorLabel, onGotoCursor
              }){
    
    const defaultClasses = useStyles();
    const [isOver, setIsOver] = useState(false);

    return (
        <ToolTip
            placement={position === 'right' ? 'left' : 'right'}
            overlay={gotoCursorLabel}
        >
            <g
                transform={`translate(${origin.x},${origin.y}) scale(2)`}
                style={{cursor: 'pointer'}}
                onClick={onGotoCursor}
                onMouseOver={() => setIsOver(true)}
                onMouseOut={() => setIsOver(false)}
            >
                <rect
                    className={clsx(
                        defaultClasses.cursorIconRect,
                        classes.cursorIconRect,
                        isOver && defaultClasses.cursorIconRectOver,
                        isOver && classes.cursorIconRectOver,
                    )}
                    width="5.5"
                    height="13.72"
                    x="9.56"
                    y="4.76" />
                <circle
                    className={clsx(
                        defaultClasses.cursorIconCircle,
                        classes.cursorIconCircle,
                        isOver && defaultClasses.cursorIconCircleOver,
                        isOver && classes.cursorIconCircleOver,
                    )}
                    cx="9.63"
                    cy="11.62"
                    r="1.4" />
                <circle
                    className={clsx(
                        defaultClasses.cursorIconCircle,
                        classes.cursorIconCircle,
                        isOver && defaultClasses.cursorIconCircleOver,
                        isOver && classes.cursorIconCircleOver,
                    )}
                    cx="15"
                    cy="11.70"
                    r="1.4" />
            </g>
        </ToolTip>
    );
}

CursorIcon.propTypes = {
    classes: PropTypes.object.isRequired,
    rcToolTipPrefixCls: PropTypes.string.isRequired,
    origin: PropTypes.shape({
        x: PropTypes.number.isRequired,
        y: PropTypes.number.isRequired
    }).isRequired,
    onGotoCursor: PropTypes.func,
    gotoCursorLabel: PropTypes.string.isRequired,
    position: PropTypes.string.isRequired,
}
