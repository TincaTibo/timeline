import React, {forwardRef, useCallback, useEffect, useRef, useState} from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/styles';
import {pointer, select} from 'd3-selection';
import {drag} from 'd3-drag';
import moment from 'moment-timezone';

import {handleHistoOver, handleHistoOut} from './handleHistoHovering';

const useStyles = makeStyles({
    dragOverlay: {
        fill: 'transparent',
        fillOpacity : 1,
        stroke: 'none',
        cursor: 'crosshair',
    }
});

function DragOverlay({
    width, height, marginBottom,
    showToolTipLeft, showToolTipRight,
    onDrawCursor, onStartDrawCursor,
    onMovedDomain, onMoveDomain,
    onEndCursor, dragOverlayRef,
    tooltipVisible, items, showHistoToolTip,
    dragging, barHovered, cursorSelection,
    setDragging, setToolTipVisible, setBarHovered,
}){
    const dragOverlayContainer = useRef();
    const [timer, setTimer] = useState(null);

    const classes = useStyles();
    const setOverlayCursor = event => {
        if(event.ctrlKey){
            dragOverlayRef.current.style.cursor = 'move';
        }
        else{
            dragOverlayRef.current.style.cursor = 'crosshair';
        }

        handleHistoOver({
            showHistoToolTip, dragging, event, dragOverlay: dragOverlayRef,
            marginBottom, items, height, tooltipVisible,
            timer, setTimer, setToolTipVisible, barHovered,
            setBarHovered
        });

    };

    useEffect(() => {
        let mode = null; //CURSOR or DOMAIN
        /* when Ctrl is pressed, we drag the domain -> move the min / max
         * when Ctrl is not pressed, we draw a cursor, starting from left
         */

        select(dragOverlayRef.current).call(drag()
            .filter((event) => !event.button)
            .container(dragOverlayContainer.current)
            .on('start',(event) => {
                if(event.sourceEvent.ctrlKey){
                    mode = 'DOMAIN';
                }
                else {
                    mode = 'CURSOR';
                    showToolTipLeft(true);
                    onStartDrawCursor(event.x);
                }
            })
            .on('drag',(event) => {
                if(mode === 'DOMAIN'){
                    onMoveDomain(event.dx);
                }
                else if(mode === 'CURSOR'){
                    showToolTipRight(true);
                    onDrawCursor(event.dx, pointer(event, dragOverlayRef.current));
                }
                setDragging(true);
            })
            .on('end',() => {
                if(mode === 'DOMAIN'){
                    onMovedDomain();
                }
                else if(mode === 'CURSOR'){
                    showToolTipLeft(false);
                    showToolTipRight(false);
                    onEndCursor();
                }
                mode = null;
                setDragging(false);
            })
        );
    }, []);

    return (
        <g ref={dragOverlayContainer}>
            <rect
                ref={dragOverlayRef}
                className={classes.dragOverlay}
                x={0}
                y={0}
                width={width}
                height={height}
                onMouseMove={setOverlayCursor}
                onMouseOver={setOverlayCursor}
                onMouseOut={handleHistoOut({setToolTipVisible, setBarHovered, timer, friendTarget: cursorSelection})}
            />
        </g>
    );
}

DragOverlay.propTypes = {
    height: PropTypes.number.isRequired,
    width: PropTypes.number.isRequired,
    marginBottom: PropTypes.number,

    items: PropTypes.arrayOf(PropTypes.shape({
        start: PropTypes.instanceOf(moment).isRequired,
        end: PropTypes.instanceOf(moment).isRequired,
        x1: PropTypes.number,
        x2: PropTypes.number,
        metrics: PropTypes.arrayOf(PropTypes.number).isRequired,
        total: PropTypes.number.isRequired,
    })),
    tooltipVisible: PropTypes.bool,
    barHovered: PropTypes.number,
    setBarHovered: PropTypes.func,
    setTooltipVisible: PropTypes.func,

    showToolTipLeft: PropTypes.func.isRequired,
    showToolTipRight: PropTypes.func.isRequired,
    showHistoToolTip: PropTypes.bool,

    onStartDrawCursor: PropTypes.func.isRequired,
    onDrawCursor: PropTypes.func.isRequired,
    onEndCursor: PropTypes.func.isRequired,

    onMoveDomain: PropTypes.func,
    onMovedDomain: PropTypes.func,
};

function dragOverlayFwd(props, ref) {
    return <DragOverlay {...props} dragOverlayRef={ref}/>
}

export default forwardRef(dragOverlayFwd);
