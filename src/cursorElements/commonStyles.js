import theme from '../theme';

export const toolTipStyles = {
    toolTipArea:{
        opacity: 0,
        visibility: 'hidden',
        transition: 'opacity visibility 0.25s'
    },
    toolTip: {
        fill: theme.colors.tipFill,
        fillOpacity: 0.80,
        stroke: theme.colors.tipText,
        strokeWidth: 0.1,
        strokeMiterlimit: 1.3,
    },
    toolTipText: {
        fontSize: theme.font.tipSize,
        fontFamily: theme.font.family,
        fill: theme.colors.tipText,
    },
};
