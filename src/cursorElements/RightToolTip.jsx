import React, {useRef, useEffect} from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/styles';

import clsx from 'clsx';
import {hide, show, resizeToolTip, resizeToolTipText, resizeToolTipRightDelta} from './utils';
import {toolTipStyles} from './commonStyles';

const useStyles = makeStyles({
    ...toolTipStyles,
    toolTipTextRight: {
        textAlign: 'end',
        textAnchor: 'end'
    }
});

export default function RightToolTip({
                                       classes, origin,
                                       isVisibleTooltipRight,
                                       stopText, height
                                   }) {
    const defaultClasses = useStyles();
    const className = (className) => clsx(defaultClasses[className], classes[className]);

    const toolTipRight = useRef();

    useEffect(() => {
        if(isVisibleTooltipRight)
            show(toolTipRight.current)
        else hide(toolTipRight.current);
    }, [isVisibleTooltipRight]);

    return (
        <g transform={`translate(${origin.x}, ${origin.y})`}
           className={className('toolTipArea')}
           ref={toolTipRight}>
            <g
                transform={`scale(-${resizeToolTip(height)},-${resizeToolTip(height)})`}
            >
                <rect
                    className={className('toolTip')}
                    x={-2.5}
                    y={-8}
                    width={stopText.length*2}
                    height={6.5}
                    rx={1}
                />
                <path
                    className={className('toolTip')}
                    d={'M -1,-1.7 0,-0.5 1,-1.7'}
                />
            </g>
            <text
                className={clsx(
                    defaultClasses.toolTipText,
                    classes.toolTipText,
                    defaultClasses.toolTipTextRight,
                    classes.toolTipTextRight,
                )}
                style={{
                    fontSize: resizeToolTipText(height) + 'px'
                }}
                x="2"
                y={resizeToolTipText(height)+resizeToolTipRightDelta(height)}
            >
                {stopText}
            </text>
        </g>
    );
};

RightToolTip.propTypes = {
    classes: PropTypes.object.isRequired,
    origin: PropTypes.shape({
        x: PropTypes.number.isRequired,
        y: PropTypes.number.isRequired
    }).isRequired,
    height: PropTypes.number.isRequired,
    isVisibleTooltipRight: PropTypes.bool.isRequired,
    stopText: PropTypes.string.isRequired,
}
