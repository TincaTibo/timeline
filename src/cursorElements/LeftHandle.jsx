import React, {useRef, useEffect} from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/styles';
import {pointer, select} from 'd3-selection';
import {drag} from 'd3-drag';

import theme from '../theme';
import clsx from 'clsx';

const useStyles = makeStyles({
    cursorLeftHandle: {
        fillOpacity: 1,
        strokeWidth: '1px',
        transition: 'r 0.5s',
        r: 4,
        '&:hover':{
            r: 8
        },
        cursor: ({maxSize}) => maxSize ? 'e-resize' : 'ew-resize',
        fill: ({startIsOutOfView}) => startIsOutOfView ? theme.colors.outsideViewFill : theme.colors.cursorHandleFill,
        stroke: ({startIsOutOfView}) => startIsOutOfView ? theme.colors.outsideViewStroke : theme.colors.cursorStroke
    },
});

export default function LeftHandle({
    classes, maxSize,
    onResizeLeftCursor, onEndResizeCursor,
    dragOverlay, showToolTipLeft,
    origin, startIsOutOfView, setDragging
}) {
    const defaultClasses = useStyles({maxSize, startIsOutOfView});
    const className = (className) => clsx(defaultClasses[className], classes[className]);

    const cursorLeftHandle = useRef();

    useEffect(() => {
        cursorLeftHandle.current &&
        select(cursorLeftHandle.current).call(
            drag()
                .filter((event) => !event.button)
                .container(dragOverlay.current)
                .on('start', () => {
                    setDragging(true)
                    showToolTipLeft(true)
                })
                .on('drag', (event) => onResizeLeftCursor(event.dx, pointer(event, dragOverlay.current)))
                .on('end', () => {
                    setDragging(false);
                    showToolTipLeft(false);
                    onEndResizeCursor();
                })
        );
    }, [cursorLeftHandle.current]);

    return (
        <circle
            ref={cursorLeftHandle}
            className={className('cursorLeftHandle')}
            r='4'
            cx={origin.x}
            cy={origin.y}
        />
    );
};

LeftHandle.propTypes = {
    classes: PropTypes.object.isRequired,
    origin: PropTypes.shape({
        x: PropTypes.number.isRequired,
        y: PropTypes.number.isRequired
    }).isRequired,
    maxSize: PropTypes.bool.isRequired,
    dragOverlay: PropTypes.object.isRequired,
    startIsOutOfView: PropTypes.bool.isRequired,
    showToolTipLeft: PropTypes.func.isRequired,
    onResizeLeftCursor: PropTypes.func.isRequired,
    onEndResizeCursor: PropTypes.func.isRequired,
    setDragging: PropTypes.func.isRequired,
}
