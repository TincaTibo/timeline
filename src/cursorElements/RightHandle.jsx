import React, {useRef, useEffect} from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/styles';
import {pointer, select} from 'd3-selection';
import {drag} from 'd3-drag';

import theme from '../theme';
import clsx from 'clsx';

const useStyles = makeStyles({
    cursorRightHandle: {
        fillOpacity: 1,
        strokeWidth: '1px',
        transition: 'r 0.5s',
        r: 4,
        '&:hover':{
            r: 8
        },
        cursor: ({maxSize}) => maxSize ? 'w-resize' : 'ew-resize',
        fill: ({endIsOutOfView}) => endIsOutOfView ? theme.colors.outsideViewFill : theme.colors.cursorHandleFill,
        stroke: ({endIsOutOfView}) => endIsOutOfView ? theme.colors.outsideViewStroke : theme.colors.cursorStroke
    },
});

export default function RightHandle({
    classes, maxSize, origin,
    onResizeRightCursor, onEndResizeCursor,
    dragOverlay, showToolTipRight, endIsOutOfView, setDragging
}) {
    const defaultClasses = useStyles({maxSize, endIsOutOfView});
    const className = (className) => clsx(defaultClasses[className], classes[className]);

    const cursorRightHandle = useRef();

    useEffect(() => {
        cursorRightHandle.current &&
        select(cursorRightHandle.current).call(
            drag()
                .filter((event) => !event.button)
                .container(dragOverlay.current)
                .on('start', () => {
                    setDragging(true);
                    showToolTipRight(true)
                })
                .on('drag', (event) => onResizeRightCursor(event.dx, pointer(event, dragOverlay.current)))
                .on('end', () => {
                    setDragging(false);
                    showToolTipRight(false);
                    onEndResizeCursor();
                })
        );
    }, [cursorRightHandle.current]);

    return (
        <circle
            ref={cursorRightHandle}
            className={className('cursorRightHandle')}
            r='4'
            cx={origin.x}
            cy={origin.y}
        />
    );
};

RightHandle.propTypes = {
    classes: PropTypes.object.isRequired,
    origin: PropTypes.shape({
        x: PropTypes.number.isRequired,
        y: PropTypes.number.isRequired
    }).isRequired,
    maxSize: PropTypes.bool.isRequired,
    dragOverlay: PropTypes.object.isRequired,
    endIsOutOfView: PropTypes.bool.isRequired,
    showToolTipRight: PropTypes.func.isRequired,
    onResizeRightCursor: PropTypes.func.isRequired,
    onEndResizeCursor: PropTypes.func.isRequired,
    setDragging: PropTypes.func.isRequired,
}
