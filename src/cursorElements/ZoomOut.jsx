import React from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/styles';
import ToolTip from '../ToolTip';

import theme from '../theme';
import clsx from 'clsx';

const zoomOutIcon = 'M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14zM7 9h5v1H7z';

const useStyles = makeStyles({
    zoomOut: {
        fill: theme.colors.zoomOutIcon,
        fillOpacity : 1,
        stroke: 'none'
    },
    zoomArea: {
        fill: 'transparent',
        fillOpacity : 1,
        stroke: 'none',
    },
    zoomOutContainer: {
        cursor: 'pointer'
    }
});

export default function ZoomOut({classes, origin, zoomOutLabel, onZoomOut, rcToolTipPrefixCls}){

    const defaultClasses = useStyles();
    const className = (className) => clsx(defaultClasses[className], classes[className]);

    return (
        <ToolTip
            placement={'left'}
            overlay={zoomOutLabel}
        >
            <g
                className={defaultClasses.zoomOutContainer}
                transform={`translate(${origin.x}, ${origin.y})`}
                onClick={onZoomOut}
            >
                <rect
                    className={defaultClasses.zoomArea}
                    x={0}
                    y={0}
                    width={23}
                    height={23}
                />
                <path
                    className={className('zoomOut')}
                    d={zoomOutIcon}
                />
            </g>
        </ToolTip>
    );
}

ZoomOut.propTypes = {
    classes: PropTypes.object.isRequired,
    rcToolTipPrefixCls: PropTypes.string.isRequired,
    origin: PropTypes.shape({
        x: PropTypes.number.isRequired,
        y: PropTypes.number.isRequired
    }).isRequired,
    zoomOutLabel: PropTypes.string,
    onZoomOut: PropTypes.func,
}
