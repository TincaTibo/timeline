import React, {forwardRef, useCallback, useEffect, useRef, useState} from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/styles';
import {pointer, select} from 'd3-selection';
import {drag} from 'd3-drag';

import theme from '../theme';
import clsx from 'clsx';
import moment from 'moment-timezone';
import {handleHistoOver, handleHistoOut} from './handleHistoHovering';

const useStyles = makeStyles({
    cursorArea: {
        fill: theme.colors.cursorFill,
        fillOpacity: 0.2,
        stroke: ({maxSize}) => maxSize ? theme.colors.maxSizeStroke : theme.colors.cursorStroke,
        strokeWidth: '1px',
        strokeLinecap: 'butt',
        strokeLinejoin: 'miter',
        strokeOpacity: 1,
        cursor: 'move',
        transition: 'fill stroke 0.5s'
    },
});

export function CursorSelection({
  classes, cursor,
  height,
  maxSize, zoomIn, canZoom,
  dragOverlay, onMoveDomain, onDragCursor, onMovedDomain, onEndDragCursor,
  showToolTipLeft, showToolTipRight, width,

  tooltipVisible, items, showHistoToolTip,
  dragging, barHovered, marginBottom,
  setDragging, setBarHovered, setToolTipVisible,
}) {
    const defaultClasses = useStyles({maxSize});
    const className = (className) => clsx(defaultClasses[className], classes[className]);

    const [timer, setTimer] = useState(null);

    const setOverlayCursor = event =>
        handleHistoOver({
            showHistoToolTip, dragging, event, dragOverlay,
            marginBottom, items, height, tooltipVisible,
            timer, setTimer, setToolTipVisible, barHovered,
            setBarHovered
        });

    useEffect(() => {
        let mode = null; //CURSOR or DOMAIN
        select(cursor.current).call(drag()
            .filter((event) => !event.button)
            .container(dragOverlay.current)
            .on('start',(event) => {
                if(event.sourceEvent.ctrlKey){
                    mode = 'DOMAIN';
                }
                else {
                    mode = 'CURSOR';
                    showToolTipRight(true);
                    showToolTipLeft(true);
                }
            })
            .on('drag', (event) => {
                if(mode === 'DOMAIN' && event.sourceEvent.ctrlKey){
                    onMoveDomain(event.dx);
                }
                else if(mode === 'CURSOR') {
                    onDragCursor(event.dx, pointer(event, dragOverlay.current));
                }
                setDragging(true);
            })
            .on('end',() => {
                if(mode === 'DOMAIN'){
                    onMovedDomain();
                }
                else if(mode === 'CURSOR'){
                    showToolTipRight(false);
                    showToolTipLeft(false);
                    onEndDragCursor();
                }
                mode = null;
                mode = null;
                setDragging(false);
            })
        );
    },
    [cursor.current]);

    return (
        <rect
            ref={cursor}
            className={className('cursorArea')}
            x={0}
            y={-3}
            width={width}
            height={height + 10}
            onDoubleClick={canZoom ? zoomIn : undefined }
            onMouseMove={setOverlayCursor}
            onMouseOver={setOverlayCursor}
            onMouseOut={handleHistoOut({setToolTipVisible, setBarHovered, timer, friendTarget: dragOverlay})}
        />
    );
};

CursorSelection.propTypes = {
    classes: PropTypes.object.isRequired,
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    maxSize: PropTypes.bool.isRequired,
    zoomIn: PropTypes.func.isRequired,
    canZoom: PropTypes.bool.isRequired,
    dragOverlay: PropTypes.object.isRequired,
    onMoveDomain: PropTypes.func.isRequired,
    onMovedDomain: PropTypes.func.isRequired,
    onDragCursor: PropTypes.func.isRequired,
    onEndDragCursor: PropTypes.func.isRequired,
    showToolTipLeft: PropTypes.func.isRequired,
    showToolTipRight: PropTypes.func.isRequired,


    items: PropTypes.arrayOf(PropTypes.shape({
        start: PropTypes.instanceOf(moment).isRequired,
        end: PropTypes.instanceOf(moment).isRequired,
        x1: PropTypes.number,
        x2: PropTypes.number,
        metrics: PropTypes.arrayOf(PropTypes.number).isRequired,
        total: PropTypes.number.isRequired,
    })),
    tooltipVisible: PropTypes.bool,
    barHovered: PropTypes.number,
    setBarHovered: PropTypes.func,
    setTooltipVisible: PropTypes.func,
}

function CursorSelectionFwd(props, ref) {
    return <CursorSelection {...props} cursor={ref}/>
}

export default forwardRef(CursorSelectionFwd);
