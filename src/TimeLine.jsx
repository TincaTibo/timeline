import React, {Component} from 'react';
import PropTypes from 'prop-types';
import moment from 'moment-timezone';
import {scaleLinear, scaleTime} from 'd3-scale';
import _merge from 'lodash-es/merge';
import _round from 'lodash-es/round';
import _max from 'lodash-es/max';
import _floor from 'lodash-es/floor';
import isEqual from 'lodash-es/isEqual';
import isFunction from 'lodash-es/isFunction';

import Cursor from './Cursor';
import Histogram from './timeLineElements/Histogram';
import XAxis from './timeLineElements/XAxis';
import YAxis from './timeLineElements/YAxis';
import Legend from './timeLineElements/Legend';
import Tools from './timeLineElements/Tools';
import HistoToolTip from './timeLineElements/HistoToolTip';
import theme, {defaultLabels, defaultQualityScale} from './theme';

import QualityLine from './timeLineElements/QualityLine';
import YGrid from './timeLineElements/YGrid';
import XGrid from './timeLineElements/XGrid';

const marginDefault = {
    left: 50,
    right: 45,
    top: 5,
    bottom: 5
};
const xAxisDefault = {
    height: 20,
    spaceBetweenTicks: 70,
    barsBetweenTicks: 8
};
const defaultZoomOutFactor = 1.25;

export default class TimeLine extends Component{
    constructor(props){
        super(props);

        this.labels = _merge(defaultLabels, props.labels);
        this.xAxis = null; //time axis computation function

        this.state = {
            isActive : false,
            start: props.timeSpan.start,
            stop: props.timeSpan.stop,
            maxTimespan: false,
            waitForLoad: false,
            tooltipVisible: false,
            barHovered: null,
            dragging: false,
            ticks: [],
            verticalMarks: [],
            ...this.computeMarginAndWidth(props)
        };

        this.state = {
            ...this.state,
            ...this.checkAndCorrectDomain({domain: props.domains[0]}),
        }

        this.widthOfLastUpdate = null;
        this.timelineElement = React.createRef();

        //manage auto sliding
        this.autoMove = null;
        this.isAutoMoving = false;
        this.lastAutoMovingDelta = 0;
        this.movedSinceLastFetched = 0;
    }

    componentDidMount(){
        if(this.state.domain){
            this.getItems(this.props, this.state.domain);
        }
        else{
            this.props.onLoadDefaultDomain();
        }
        this.computeMarginAndWidth(this.props);
    }

    componentDidUpdate(prevProps, prevState) {
        //reached max selection
        if(!prevState.maxTimespan && this.state.maxTimespan){
            this.props.onShowMessage(this.labels.maxSelectionMsg);
        }

        //Change of width (big): update item
        if((prevProps.width !== this.props.width) || !isEqual(prevProps.margin, this.props.margin)){
            if(this.state.isActive){
                this.getItems(this.props, this.state.domain, this.widthOfLastUpdate && Math.abs(this.props.width - this.widthOfLastUpdate) > 30);
                this.setState(this.computeMarginAndWidth(this.props));
            }
            else{
                this.setState(this.computeMarginAndWidth(this.props));
            }

            if(this.state.domain){

                const {maxZoom, minZoom, domain, domainHasChanged, minTime, maxTime} = this.checkAndCorrectDomain({domain: this.state.domain});
                if(maxZoom !== this.state.maxZoom || minZoom !== this.state.minZoom){

                    this.setState({
                        maxZoom,
                        minZoom,
                        minTime,
                        maxTime,
                        domain: domainHasChanged ? domain: this.state.domain
                    });
                    if(domainHasChanged){
                        const domains = [domain, ...this.props.domains.slice(1)];
                        this.props.onUpdateDomains(domains);
                    }
                }
            }
        }

        //If selection changed
        if(!prevProps.timeSpan.start.isSame(this.props.timeSpan.start)
            || !prevProps.timeSpan.stop.isSame(this.props.timeSpan.stop)){

            const newDomains = this.shiftDomains(this.props.domains, {
                min: this.props.timeSpan.start,
                max: this.props.timeSpan.stop
            });

            if(newDomains !== this.props.domains){
                this.props.onUpdateDomains(newDomains);
            }

            //Update cursor
            this.setState({
                start: this.props.timeSpan.start,
                stop: this.props.timeSpan.stop
            });
        }

        //If change in current domain, update items and state
        if((!prevProps.domains[0] && this.props.domains[0])
            || (prevProps.domains[0]
                && (!prevProps.domains[0].min.isSame(this.props.domains[0].min)
                    || !prevProps.domains[0].max.isSame(this.props.domains[0].max)))){

            //If current zoom changes and is changing min/max zoom attribute
            //then update min/maxZoom and crop domain
            const {maxZoom, minZoom, domain, domainHasChanged, minTime, maxTime} = this.checkAndCorrectDomain({domain: this.props.domains[0]});
            this.setState({
                maxZoom,
                minZoom,
                minTime,
                maxTime,
                domain
            });

            //if domain changed, update domain, and getItems will be done later
            if(domainHasChanged){
                const domains = [domain, ...this.props.domains.slice(1)];
                this.props.onUpdateDomains(domains);
            }
            else{
                this.getItems(this.props, domain);
            }
        }

        //If we got new histo change indicator
        if(prevProps.histo !== this.props.histo){
            this.setState({
                waitForLoad: false,
            });
        }

        //Recompute margin on legend change
        if(prevProps.metricsDefinition.legends !== this.props.metricsDefinition.legends){
            this.computeMarginAndWidth(this.props);
        }
    }

    computeMarginAndWidth(props){
        const {margin, showLegend, metricsDefinition, width} = this.props;

        const localMargin = {
            ...marginDefault,
            ...margin
        }

        if(showLegend){
            const maxLength = _max(metricsDefinition.legends.map(s => s.length));
            localMargin.left = _max([5 + maxLength*9, localMargin.left]);
        }

        return {
            margin: localMargin,
            histoWidth: width - localMargin.left - localMargin.right
        }
    }

    getItems(props, domain, shouldReload = true){
        const histoWidth = this.computeMarginAndWidth(this.props).histoWidth;

        this.xAxis = scaleTime()
            .domain([domain.min, domain.max])
            .range([0, histoWidth]);
        this.xAxis.clamp(true);

        const ticks = this.xAxis.ticks(_floor(histoWidth / props.xAxis.spaceBetweenTicks));

        const intervalMs = _max([_round(moment(ticks[1]).diff(moment(ticks[0]))/props.xAxis.barsBetweenTicks), this.props.smallestResolution.asMilliseconds()]);

        this.setState({
            histoWidth,
            isActive: true,
            ticks
        });

        if(shouldReload && intervalMs){
            this.widthOfLastUpdate = props.width;
            this.setState({
                waitForLoad: true
            });
            props.onLoadHisto(intervalMs, domain.min, domain.max);
        }
    }

    prepareVerticalScale({yAxis, xAxis, height, histo, margin}){
        const items = histo && histo.items;

        const maxHeight = height - margin.bottom - margin.top - (xAxis.height || xAxisDefault.height);

        //Y Axis computation
        const maxScale = items ? _max(items.map(i => i.total)) || 0 : 0;
        const verticalScale = scaleLinear()
            .domain([0, maxScale])
            .range([0, maxHeight]);

        const verticalMarks = yAxis.spaceBetweenTicks && (maxHeight>yAxis.spaceBetweenTicks*2) ?
            verticalScale.ticks(_floor(maxHeight / yAxis.spaceBetweenTicks))
            : [maxScale];

        return {
            verticalScale,
            verticalMarks,
            maxHeight
        };
    }

    prepareHistogram({histo, domain, verticalScale}){
        if(histo && histo.items && domain){
            const {min, max} = domain;

            return histo.items
                .filter(item => item.total > 0
                    && item.time.isSameOrAfter(min)
                    && item.time.isBefore(max))
                .map(item => {
                    const start = moment(item.time);
                    const end = moment(item.time).add(histo.intervalMs);
                    return {
                        x1: this.xAxis(start),
                        x2: this.xAxis(end),
                        start,
                        end,
                        metrics: item.metrics,
                        total: item.total,
                        height: verticalScale(item.total)
                    }
                });
        }
        else {
            return [];
        }
    }

    render() {
        const {width, height, histo, tools, quality, qualityScale, metricsDefinition,
            classes, rcToolTipPrefixCls, showLegend,
            onFormatTimeLegend, onFormatMetricLegend, onResetTime, onFormatTimeToolTips,
            xAxis, yAxis, showHistoToolTip, HistoToolTip
        } = this.props;
        const {isActive, domain, histoWidth, margin,
            start, stop, maxZoom, minZoom,
            maxTimespan, barHovered, tooltipVisible,
            ticks, dragging
        } = this.state;
        const xAxisHeight = (xAxis.height || xAxisDefault.height);
        const pointZero = {
            x: 0,
            y: height - margin.bottom - xAxisHeight
        };
        const originCursor = {
            x: 0,
            y: margin.top - 5
        }

        if(isFunction(this.xAxis)){
            const {verticalScale, verticalMarks, maxHeight} = this.prepareVerticalScale({yAxis, xAxis, height, histo, margin});
            const items = this.prepareHistogram({histo, domain, verticalScale});
            this.items = items;

            return (
                <svg
                    ref={this.timelineElement}
                    width={width}
                    height={height}
                    onWheel={isActive ? this.onWheel : undefined}
                >
                    <g transform={`translate(${margin.left},${margin.top})`}>
                        {
                            yAxis.showGrid &&
                            <YGrid
                                classes={classes}
                                origin={pointZero}
                                yAxis={verticalScale}
                                marks={verticalMarks}
                                xAxisWidth={histoWidth + 13}
                            />
                        }
                        {
                            xAxis.showGrid &&
                            <XGrid
                                classes={classes}
                                origin={pointZero}
                                xAxis={this.xAxis}
                                marks={ticks}
                                yAxisHeight={maxHeight}
                            />
                        }
                        {
                            isActive &&
                            <Histogram
                                classes={classes}
                                origin={pointZero}
                                items={items}
                                histo={histo}
                                verticalScale={verticalScale}
                                metricsDefinition={metricsDefinition}
                                barHovered={barHovered}
                                dragging={dragging}
                                tooltipVisible={tooltipVisible}
                                onFormatTimeToolTips={onFormatTimeToolTips}
                                onFormatMetricLegend={onFormatMetricLegend}
                                HistoToolTip={HistoToolTip}
                            />
                        }
                        <XAxis
                            classes={classes}
                            origin={pointZero}
                            axisWidth={histoWidth + 13}
                            min={domain && domain.min}
                            max={domain && domain.max}
                            xAxis={this.xAxis}
                            marks={ticks}
                            arrowPath={xAxis.arrowPath}
                            yAxisHeight={maxHeight}
                            showGrid={xAxis.showGrid}
                            onFormatTimeLegend={onFormatTimeLegend}
                        />
                        <Tools
                            classes={classes}
                            rcToolTipPrefixCls={rcToolTipPrefixCls}
                            origin={pointZero}
                            histoWidth={histoWidth}
                            isActive={isActive}
                            tools={tools}
                            labels={this.labels}
                            onGoto={this.onGoto}
                            onResetTime={onResetTime}
                            shiftTimeLine={this.shiftTimeLine}
                        />
                        <YAxis
                            classes={classes}
                            origin={pointZero}
                            maxHeight={maxHeight}
                            yAxis={verticalScale}
                            marks={verticalMarks}
                            arrowPath={yAxis.arrowPath}
                            showGrid={yAxis.showGrid}
                            xAxisWidth={histoWidth + 13}
                            onFormatMetricLegend={onFormatMetricLegend}
                        />
                        {showLegend &&
                            <Legend
                                classes={classes}
                                origin={{
                                    x: -5,
                                    y: margin.top + 2
                                }}
                                metricsDefinition={metricsDefinition}
                            />
                        }
                        { isActive && tools.cursor!==false &&
                            <Cursor
                                origin = {originCursor}
                                rcToolTipPrefixCls={rcToolTipPrefixCls}
                                classes={classes}

                                startPos={this.xAxis(start)}
                                startIsOutOfView={start.isBefore(domain.min) || start.isAfter(domain.max)}

                                endPos={this.xAxis(stop)}
                                endIsOutOfView={stop.isBefore(domain.min) || stop.isAfter(domain.max)}

                                cursorIsBeforeView={start.isBefore(domain.min) && stop.isBefore(domain.min)}
                                cursorIsAfterView={start.isAfter(domain.max) && stop.isAfter(domain.max)}

                                height={maxHeight}

                                overlayHeight={height - margin.top}
                                overlayWidth={histoWidth}
                                tooltipVisible={tooltipVisible}
                                barHovered={barHovered}
                                setToolTipVisible={tooltipVisible => this.setState({tooltipVisible})}
                                setBarHovered={barHovered => this.setState({barHovered})}
                                items={items}
                                showHistoToolTip={showHistoToolTip}
                                dragging={dragging}
                                setDragging={(dragging) => this.setState({dragging})}

                                canZoom={true}
                                minZoom={minZoom}
                                maxZoom={maxZoom}

                                startText={onFormatTimeToolTips(start)}
                                stopText={onFormatTimeToolTips(stop)}

                                maxSize={maxTimespan}

                                tools={tools}

                                gotoCursorLabel={this.labels.gotoCursor}
                                zoomInLabel={this.labels.zoomInLabel}
                                zoomOutLabel={this.labels.zoomOutLabel}

                                zoomIn={this.zoomIn}
                                zoomOut={this.zoomOut}
                                onResizeLeftCursor={this.onResizeLeftCursor}
                                onResizeRightCursor={this.onResizeRightCursor}
                                onEndResizeCursor={this.onEndChangeCursor}
                                onDragCursor={this.onDragCursor}
                                onEndDragCursor={this.onEndChangeCursor}
                                onStartDrawCursor={this.onStartDrawCursor}
                                onDrawCursor={this.onDrawCursor}
                                onEndCursor={this.onEndDrawCursor}
                                onMoveDomain={this.moveTimeLine}
                                onMovedDomain={this.movedTimeLine}
                                onGotoCursor={this.onGotoCursor}
                            />
                        }
                        { isActive && quality && quality.items &&
                            <QualityLine
                                classes={classes}
                                rcToolTipPrefixCls={rcToolTipPrefixCls}
                                origin={pointZero}
                                quality={quality}
                                qualityScale={qualityScale}
                                xAxis={this.xAxis}
                            />
                        }
                    </g>
                </svg>
            )
        }
        else{
            return null;
        }
    }

    shiftDomains = (domains, {min, max}) => {
        //update domains if selection is outside domains
        let toUpdate = false;
        const biggestVisibleDomain = this.props.biggestVisibleDomain;
        const newDomains = domains.map((domain, index) => {
            if((min && min.isBefore(domain.min)) || (max && max.isAfter(domain.max))){
                toUpdate = true;
                if(index === domains.length - 1){
                    //if last domain, increase domain
                    const newDomain = {
                        min: min && min.isBefore(domain.min) ? min : domain.min,
                        max: max && max.isAfter(domain.max) ? max : domain.max
                    };

                    if(biggestVisibleDomain && moment(newDomain.min).add(biggestVisibleDomain).isSameOrBefore(newDomain.max)){
                        if(min.isBefore(domain.min)){
                            newDomain.max = moment(min).add(biggestVisibleDomain);
                        }
                        else {
                            newDomain.min = moment(max).subtract(biggestVisibleDomain);
                        }
                    }

                    return newDomain;
                }
                else{
                    //if not last domain, shift domain
                    if(min && min.isBefore(domain.min)){
                        return {
                            min,
                            max: moment(domain.max).subtract(domain.min.diff(min))
                        }
                    }
                    else{
                        return {
                            min: moment(domain.min).add(max.diff(domain.max)),
                            max
                        }
                    }

                }
            }
            else{
                return domain;
            }
        });
        return toUpdate ? newDomains : domains;
    }

    onResizeLeftCursor = (delta, mouse) => {
        let newStart = moment(this.xAxis.invert(this.xAxis(this.state.start) + delta));
        let newStop = this.state.stop;
        let maxTimespan = false;

        if (newStart.isSameOrAfter(newStop)) {
            newStop = moment(this.xAxis.invert(this.xAxis(newStop) + delta));
        }

        if(this.props.biggestTimeSpan){
            const min = moment(newStop).subtract(this.props.biggestTimeSpan);
            if(min.isSameOrAfter(newStart)){
                newStart = min;
                maxTimespan = true;
            }
        }

        this.handleAutoSliding(mouse, this.onResizeLeftCursor);

        if ((newStop !== this.state.stop
            && newStop.isSameOrBefore(this.state.domain.max))
            || newStart.isSameOrAfter(this.state.domain.min)) {
            this.setState({
                start: newStart,
                stop: newStop,
                maxTimespan
            });
        }
    };

    onResizeRightCursor = (delta, mouse) =>  {
        let newStop = moment(this.xAxis.invert(this.xAxis(this.state.stop) + delta));
        let newStart = this.state.start;
        let maxTimespan = false;

        if(newStop.isSameOrBefore(newStart)){
            newStart = moment(this.xAxis.invert(this.xAxis(newStart) + delta));
        }

        if(this.props.biggestTimeSpan){
            const max = moment(newStart).add(this.props.biggestTimeSpan);
            if(max.isSameOrBefore(newStop)){
                newStop = max;
                maxTimespan = true;
            }
        }

        this.handleAutoSliding(mouse, this.onResizeRightCursor);

        if (newStop.isSameOrBefore(this.state.domain.max) &&
            newStart.isSameOrAfter(this.state.domain.min)){
            this.setState({
                start: newStart,
                stop: newStop,
                maxTimespan
            });
        }
    };

    onDragCursor = (delta, mouse) => {
        this.xAxis.clamp(false);
        const duration = this.state.stop.diff(this.state.start);
        let newStart = moment(this.xAxis.invert(this.xAxis(this.state.start) + delta));
        let newStop = moment(newStart).add(duration, 'MILLISECONDS');
        this.xAxis.clamp(true);

        const {maxDomain} = this.props;
        if(maxDomain.min && newStart.isBefore(maxDomain.min)){
            newStart = moment(maxDomain.min);
        }
        if(maxDomain.max && newStop.isAfter(maxDomain.max)){
            newStop = moment(maxDomain.max);
        }

        this.handleAutoSliding(mouse, this.onDragCursor);

        this.setState({
            start: newStart,
            stop: newStop
        });
    };

    onStartDrawCursor = (pos) => {
        this.setState({
            start: moment(this.xAxis.invert(pos)),
            stop: moment(this.xAxis.invert(pos + 1)),
            maxTimespan: false
        });
    };

    onDrawCursor = (delta, mouse) => {
        let newStop = moment(this.xAxis.invert(this.xAxis(this.state.stop) + delta));
        let newStart = this.state.start;
        let maxTimespan = false;

        if(newStop.isSameOrBefore(newStart)){
            newStart = moment(this.xAxis.invert(this.xAxis(newStart) + delta));
        }

        if(this.props.biggestTimeSpan){
            const max = moment(newStart).add(this.props.biggestTimeSpan);
            if(max.isSameOrBefore(newStop)){
                newStop = max;
                maxTimespan = true;
            }
        }

        this.handleAutoSliding(mouse, this.onDrawCursor);

        if (newStop.isSameOrBefore(this.state.domain.max) &&
            newStart.isSameOrAfter(this.state.domain.min)){
            this.setState({
                start: newStart,
                stop: newStop,
                maxTimespan
            });
        }
    };

    stopAutoMove = () => {
        if(this.autoMove) {
            this.setAutoMove(false);
        }
        if(this.isAutoMoving){
            this.isAutoMoving = false;
            this.movedTimeLine();
        }
    }

    onEndDrawCursor = () => {
        this.stopAutoMove();

        const {onCustomRange, selectBarOnClick} = this.props;
        let {start, stop, barHovered} = this.state;

        if(selectBarOnClick && this.items && barHovered > -1){
            const item = this.items[barHovered];
            if(start.isSameOrAfter(item.start) && stop.isSameOrBefore(item.end)){
                start = moment(item.start);
                stop = moment(item.end);
                this.setState({start, stop});
            }
        }

        onCustomRange(start, stop);
    };

    onEndChangeCursor = () => {
        this.stopAutoMove();

        const {onCustomRange} = this.props;
        const {start, stop} = this.state;
        onCustomRange(start, stop);
    };

    handleAutoSliding = (mouse, action) => {
        //slide domain when dragging and mouse out of histo
        if(mouse){
            const posX = mouse[0];
            if(posX < 0){
                this.setAutoMove(true, action, posX);
            }
            else if(posX >= 0 && posX <= this.state.histoWidth){
                this.setAutoMove(false);
            }
            else if(posX > this.state.histoWidth ){
                this.setAutoMove(true, action, posX - this.state.histoWidth);
            }
        }
    }

    setAutoMove = (active, action, delta) => {
        if(active){
            //prevent creating timer to often if mouse did not move so much
            if(!this.autoMove || (this.autoMove && Math.abs(this.lastAutoMovingDelta - delta) > 5)){
                clearInterval(this.autoMove);
                this.autoMove = setInterval(() => {
                    action(delta);
                    this.moveTimeLine(-delta);
                    this.isAutoMoving = true;
                    this.lastAutoMovingDelta = delta;
                },30);
            }
        }
        else if(!active){
            clearInterval(this.autoMove);
            this.autoMove = null;
        }
    }

    onWheel = (event) => {
        const {waitForLoad, maxZoom, minZoom} = this.state;
        const {onShowMessage, domains} = this.props;

        event.stopPropagation();
        const baseX = this.timelineElement.current.getBoundingClientRect().left;
        const x = event.clientX - baseX - this.state.margin.left;

        if(!waitForLoad){
            if(event.deltaY < -50){
                if(maxZoom){
                    onShowMessage(this.labels.scrollMaxZoomMsg);
                }
                else{
                    this.zoomOnTarget(x);
                }
            }
            else if(event.deltaY > 50){
                if(minZoom){
                    onShowMessage(this.labels.minZoomMsg);
                }
                else if(domains.length > 1) {
                    this.zoomOut();
                }
                else {
                    this.zoomOutOfTarget(x);
                }
            }
        }
    };

    onGotoCursor = () => {
        const middle = moment(this.state.start).add(this.state.stop.diff(this.state.start)/2);
        this.xAxis.clamp(false);
        const currentX = this.state.histoWidth/2;
        const newX = this.xAxis(middle);

        const newDomain = this.moveTimeLineCore(currentX - newX);

        const domains = [...this.props.domains];
        //Replace current domain
        domains.splice(0,1,newDomain);

        //Check if other domains should shift
        const newDomains = this.shiftDomains(domains, newDomain);
        this.props.onUpdateDomains(newDomains);
        this.xAxis.clamp(true);
    };

    onGoto = (d) => {
        const halfTimeAgg = this.state.stop.diff(this.state.start)/2;
        const when = d || moment();
        const newStop = moment(when).add(halfTimeAgg);
        const newStart = moment(when).subtract(halfTimeAgg);

        this.props.onCustomRange(newStart, newStop);
    };

    zoomOnTarget = (x) => {
        const [minOrigin, maxOrigin] = this.xAxis.domain();
        const target = moment(this.xAxis.invert(x));

        const min = moment(target).subtract(0.25 * (target.diff(moment(minOrigin))));
        const max = moment(target).add(0.25 * (moment(maxOrigin).diff(target)));

        this.zoomOnDomain({
            min,
            max
        });
    };

    zoomOutOfTarget = (x) => {
        const [minOrigin, maxOrigin] = this.xAxis.domain();
        const target = moment(this.xAxis.invert(x));

        const min = moment(target).subtract(1.25 * (target.diff(moment(minOrigin))));
        const max = moment(target).add(1.25 * (moment(maxOrigin).diff(target)));

        this.zoomOutTo([{min, max}]);
    }

    checkAndCorrectDomain = ({domain}) => {
        if(!domain){
            return {
                domain,
                maxZoom: false,
                minZoom: false,
                domainHasChanged: false
            }
        }

        const newDomain = {...domain};
        const {biggestVisibleDomain, maxDomain, smallestResolution} = this.props;
        const {histoWidth} = this.state;

        let domainHasChanged = false;

        //Maximum zoom?
        const duration = newDomain.max.diff(newDomain.min);
        let maxZoom = false;

        //We stop/recap zoom in when 15px = 1min
        const minRes = smallestResolution.asMilliseconds();
        if(duration <= (histoWidth*minRes/15)) {
            maxZoom = true;
            newDomain.max = moment(newDomain.min).add(histoWidth*minRes/15, 'ms');
        }

        //Minimum zoom?
        let minTime, maxTime;
        if(maxDomain.min && newDomain.min.isBefore(maxDomain.min)){
            newDomain.min = moment(maxDomain.min);
            minTime = true;
        }
        if(maxDomain.max && newDomain.max.isAfter(maxDomain.max)){
            newDomain.max = moment(maxDomain.max);
            maxTime = true;
        }

        let minZoom = false;
        if(biggestVisibleDomain && newDomain.max.isSameOrAfter(moment(newDomain.min).add(biggestVisibleDomain))){
            minZoom = true;
            newDomain.min = moment(newDomain.max).subtract(biggestVisibleDomain);
        }

        if(! (newDomain.min.isSame(domain.min) && newDomain.max.isSame(domain.max)) ){
            domainHasChanged = true;
        }

        return {
            domain: domainHasChanged ? newDomain : domain,
            maxZoom,
            minZoom,
            maxTime,
            minTime,
            domainHasChanged
        }
    }

    zoomOnDomain = (targetDomain) => {
        const {onShowMessage, onUpdateDomains} = this.props;

        if(!this.state.waitForLoad) {
            if(! (targetDomain.min.isSame(this.state.domain.min) && targetDomain.max.isSame(this.state.domain.max))){

                const {maxZoom, domain} = this.checkAndCorrectDomain({domain: targetDomain});
                const {maxZoom: currentMaxZoom} = this.state;

                if(maxZoom && maxZoom !== currentMaxZoom){
                    onShowMessage(this.labels.zoomSelectionResolutionExtended);
                }

                const domains = [domain, ...this.props.domains];

                this.setState({
                    maxZoom,
                    minZoom: false,
                    domain
                });
                onUpdateDomains(domains);
            }
            else{
                onShowMessage(this.labels.zoomInWithoutChangingSelectionMsg);
            }
        }
    };

    zoomOutTo = (domains) => {
        const {minZoom, domain, domainHasChanged, maxTime, minTime} = this.checkAndCorrectDomain({domain: domains[0]});
        const {onShowMessage, onUpdateDomains} = this.props;

        if(minZoom){
            onShowMessage(this.labels.minZoomMsg);
        }
        if(minTime){
            onShowMessage(this.labels.minDomainMsg);
        }
        if(maxTime){
            onShowMessage(this.labels.maxDomainMsg);
        }

        const newDomains = domainHasChanged ? [domain, ...this.props.domains.slice(1)] : domains;

        this.setState({
            maxZoom: false,
            minZoom,
            domain
        });
        onUpdateDomains(newDomains);
    };

    zoomIn = () => {
        const {maxZoom, start, stop} = this.state;
        const {onShowMessage} = this.props;

        if(!maxZoom){
            this.zoomOnDomain({
              min: start,
              max: stop
            })
        }
        else{
            onShowMessage(this.labels.doubleClickMaxZoomMsg);
        }
    };

    zoomOut = () => {
        if(!this.state.waitForLoad && !this.state.minZoom){
            const domains = [...this.props.domains];
            const {zoomOutFactor} = this.props;

            if(domains.length > 1){
                domains.shift();
            }
            else{
                const [minOrigin, maxOrigin] = this.xAxis.domain();
                const halfDuration = moment(maxOrigin).diff(moment(minOrigin))/2;
                const target = moment(minOrigin).add(halfDuration);

                const min = moment(target).subtract(zoomOutFactor * halfDuration);
                const max = moment(target).add(zoomOutFactor * halfDuration);

                domains[0] = {min, max};
            }
            this.zoomOutTo(domains);
        }
    };

    moveTimeLineCore = (delta) => {
        const {maxDomain, onShowMessage} = this.props;
        let {domain: currentDomain, minTime = false, maxTime = false} = this.state;

        const currentSpan = currentDomain.max.diff(currentDomain.min);

        if((minTime && (delta > 0)) || (maxTime && (delta < 0))){
            return {
                ...currentDomain,
                moved: 0,
                minTime,
                maxTime
            }
        }
        else{
            let min = moment(this.xAxis.invert(-delta));
            let max = moment(this.xAxis.invert(this.state.histoWidth - delta));
            let moved = delta;

            if(maxDomain.min && min.isBefore(maxDomain.min)){
                moved = this.xAxis(maxDomain.min);
                min = moment(maxDomain.min);
                max = moment(min).add(currentSpan);
                minTime = true;
                onShowMessage(this.labels.minDomainMsg);
            }

            if(maxDomain.max && max.isAfter(maxDomain.max)){
                moved = this.xAxis(maxDomain.max)
                max = moment(maxDomain.max);
                min = moment(max).subtract(currentSpan);
                maxTime = true;
                onShowMessage(this.labels.maxDomainMsg);
            }

            return {min, max, moved, minTime, maxTime};
        }
    }

    moveTimeLine = (delta) => {
        this.xAxis.clamp(false);
        const {min, max, moved, minTime, maxTime} = this.moveTimeLineCore(delta);

        if(moved !== 0){
            this.xAxis.domain([min, max]);
            this.xAxis.clamp(true);

            const ticks = this.xAxis.ticks(_floor(this.state.histoWidth / this.props.xAxis.spaceBetweenTicks));

            this.movedSinceLastFetched += moved;
            if(Math.abs(this.movedSinceLastFetched) > 75 && this.props.fetchWhileSliding){
                this.movedSinceLastFetched = 0;
                this.getItems(this.props, {min, max});
            }

            this.setState({
                domain: {
                    min,
                    max,
                },
                minTime,
                maxTime,
                ticks
            });
        }
    };

    movedTimeLine = () => {
        this.movedSinceLastFetched = 0;
        const domain = this.state.domain;
        const {domains: originalDomains, onUpdateDomains} = this.props;

        //Replace current domain
        const domains = [domain, ...originalDomains.slice(1)];

        //Check if other domains should shift
        const newDomains = this.shiftDomains(domains, domain);
        onUpdateDomains(newDomains);
    };

    shiftTimeLine = (delta) => {
        const incr = delta/Math.abs(delta)*8;
        let i = 0;
        function step(){
            this.moveTimeLine(incr);
            i += 8;
            if(i < Math.abs(delta)){
                setTimeout(step.bind(this), 10);
            }
            else{
                this.movedTimeLine();
            }
        }
        step.bind(this)();
    };

    static propTypes = {
        className: PropTypes.string,
        classes: PropTypes.object,
        rcToolTipPrefixCls: PropTypes.string,

        timeSpan: PropTypes.shape({
            start: PropTypes.instanceOf(moment).isRequired,
            stop: PropTypes.instanceOf(moment).isRequired
        }).isRequired,
        domains: PropTypes.arrayOf(PropTypes.shape({
            min: PropTypes.instanceOf(moment).isRequired,
            max: PropTypes.instanceOf(moment).isRequired
        })).isRequired,
        maxDomain: PropTypes.shape({
            min: PropTypes.instanceOf(moment),
            max: PropTypes.instanceOf(moment)
        }),
        biggestVisibleDomain: PropTypes.object,
        smallestResolution: PropTypes.object.isRequired,
        biggestTimeSpan: PropTypes.object,
        histo: PropTypes.shape({
            items: PropTypes.arrayOf(PropTypes.shape({
                time: PropTypes.instanceOf(moment).isRequired,
                metrics: PropTypes.arrayOf(PropTypes.number).isRequired,
                total: PropTypes.number.isRequired,
            })),
            intervalMs: PropTypes.number
        }).isRequired,
        showHistoToolTip: PropTypes.bool,
        HistoToolTip: PropTypes.elementType,
        quality: PropTypes.shape({
            items: PropTypes.arrayOf(PropTypes.shape({
                time: PropTypes.instanceOf(moment).isRequired,
                quality: PropTypes.number.isRequired,
                tip: PropTypes.node
            })),
            intervalMin: PropTypes.number
        }),
        qualityScale: PropTypes.func,
        zoomOutFactor: PropTypes.number,
        metricsDefinition: PropTypes.shape({
            count: PropTypes.number.isRequired,
            legends: PropTypes.arrayOf(PropTypes.string).isRequired,
            colors: PropTypes.arrayOf(PropTypes.shape({
                fill: PropTypes.string.isRequired,
                stroke: PropTypes.string.isRequired,
                text: PropTypes.string.isRequired,
            })).isRequired
        }).isRequired,
        width: PropTypes.number,
        height: PropTypes.number,
        labels: PropTypes.shape({
            forwardButtonTip: PropTypes.string,
            backwardButtonTip: PropTypes.string,
            resetButtonTip: PropTypes.string,
            gotoNowButtonTip: PropTypes.string,
            doubleClickMaxZoomMsg: PropTypes.string,
            zoomInWithoutChangingSelectionMsg: PropTypes.string,
            zoomSelectionResolutionExtended: PropTypes.string,
            maxSelectionMsg: PropTypes.string,
            scrollMaxZoomMsg: PropTypes.string,
            minZoomMsg: PropTypes.string,
            maxDomainMsg: PropTypes.string,
            minDomainMsg: PropTypes.string,
            gotoCursor: PropTypes.string,
            zoomInLabel: PropTypes.string,
            zoomOutLabel: PropTypes.string,
        }),
        showLegend: PropTypes.bool,
        tools: PropTypes.shape({
            slideForward: PropTypes.bool,
            slideBackward: PropTypes.bool,
            resetTimeline: PropTypes.bool,
            gotoNow: PropTypes.bool,
            cursor: PropTypes.bool,
            zoomIn: PropTypes.bool,
            zoomOut: PropTypes.bool,
        }),
        fetchWhileSliding: PropTypes.bool,
        selectBarOnClick: PropTypes.bool,

        xAxis: PropTypes.shape({
            arrowPath: PropTypes.string,
            spaceBetweenTicks: PropTypes.number.isRequired,
            barsBetweenTicks: PropTypes.number.isRequired,
            showGrid: PropTypes.bool,
            height: PropTypes.number,
        }),
        yAxis: PropTypes.shape({
            path: PropTypes.string,
            spaceBetweenTicks: PropTypes.number,
            showGrid: PropTypes.bool,
        }),
        margin: PropTypes.shape({
            left: PropTypes.number,
            right: PropTypes.number,
            top: PropTypes.number,
            bottom: PropTypes.number,
        }),

        onLoadDefaultDomain: PropTypes.func.isRequired,
        onLoadHisto: PropTypes.func.isRequired,
        onCustomRange: PropTypes.func.isRequired,
        onShowMessage: PropTypes.func.isRequired,
        onUpdateDomains: PropTypes.func.isRequired,
        onResetTime: PropTypes.func.isRequired,
        onFormatTimeToolTips: PropTypes.func.isRequired,
        onFormatTimeLegend: PropTypes.func.isRequired,
        onFormatMetricLegend: PropTypes.func.isRequired,
    };

    static defaultProps = {
        classes: {},
        rcToolTipPrefixCls: theme.rcToolTipPrefixCls,
        tools: {
            slideForward: true,
            slideBackward: true,
            resetTimeline: true,
            gotoNow: true,
            cursor: true,
            zoomIn: true,
            zoomOut: true
        },
        xAxis: xAxisDefault,
        yAxis: {},
        margin: marginDefault,
        showLegend: true,
        qualityScale: defaultQualityScale,
        zoomOutFactor: defaultZoomOutFactor,
        HistoToolTip: HistoToolTip
    }
}
