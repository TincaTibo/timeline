import {scaleLinear} from 'd3-scale';
import './tipDark.css';

const theme = {
    colors: {
        light: '#ececec',
        medium: '#b4b4b4',
        mediumDark: '#545454',
        dark: '#333333',
        itemFillSelected: '#ff8080',
        itemStrokeSelected: '#e7594f',
        buttonOver: '#ff8080',
        qualityFillSelected: '#00ebeb',
        zoomOutIcon: '#ff816b',
        zoomInIcon: '#00BCD4',
        maxSizeStroke: '#00899f',
        cursorHandleFill: '#ffffff',
        cursorStroke: '#00BCD4',
        cursorFill: '#00BCD4',
        outsideViewFill: '#ececec',
        outsideViewStroke: '#b4b4b4',
        tipFill: '#1a1a1a',
        tipText: '#ffffff'
    },
    font: {
        family: 'Roboto, sans-serif',
        axisTextSize: 10,
        legendSize: 12.5,
        tipSize: 12.5
    },
    rcToolTipPrefixCls: 'rc-tooltip-dark'
};
export default theme;

export const defaultLabels = {
    forwardButtonTip: 'Slide forward',
    backwardButtonTip:  'Slide backward',
    resetButtonTip: 'Reset time span',
    gotoNowButtonTip: 'Goto Now',
    doubleClickMaxZoomMsg: 'Cannot zoom anymore!',
    scrollMaxZoomMsg: 'Cannot zoom anymore!',
    zoomInWithoutChangingSelectionMsg: 'Please change time selection before clicking on zoom ;)',
    zoomSelectionResolutionExtended: 'You reached maximum zoom level',
    minZoomMsg: 'You reached minimum zoom level',
    maxSelectionMsg: 'You reached maximum selection',
    maxDomainMsg: 'You reached maximum visible time',
    minDomainMsg: 'You reached minimum visible time',
    gotoCursor: 'Goto Cursor',
    zoomInLabel: 'Zoom in',
    zoomOutLabel: 'Zoom out',
};

export const defaultQualityScale = scaleLinear()
    .domain([0, 0.9, 1])
    .range(['#ff3724', 'rgba(255,0,0,0.09)', 'rgba(155,249,135,0.94)'])
    .clamp(true);
