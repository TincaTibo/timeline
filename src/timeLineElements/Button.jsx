import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/styles';
import ToolTip from '../ToolTip';
import clsx from 'clsx';

import theme from '../theme';

const useStyles = makeStyles({
    buttonGroup: {
        cursor: 'pointer'
    },
    buttonGroupInactive: {
        cursor: 'not-allowed'
    },
    buttonOverShape: {
        fill: 'transparent'
    },
    buttonIcon: {
        fill: theme.colors.medium,
        fillOpacity: 1,
        stroke: theme.colors.medium,
        strokeWidth: 1,
        strokeOpacity: 1
    },
    buttonIconOver: {
        fill: theme.colors.buttonOver,
        stroke: theme.colors.buttonOver,
    }
});

export default function Button({tipPlacement, tipOverlay, path, origin, scale, iconSize, onClick, classes, rcToolTipPrefixCls, isActive}){
    const [isOver, setOver] = useState(false);
    const defaultClasses = useStyles();

    return (
        <ToolTip
            placement={tipPlacement}
            overlay={tipOverlay}
        >
            <g
                className={clsx(
                    defaultClasses.buttonGroup,
                    classes.buttonGroup,
                    !isActive && defaultClasses.buttonGroupInactive,
                    !isActive && classes.buttonGroupInactive,
                )}
                transform={`translate(${origin.x},${origin.y}) scale(${scale})`}
                onMouseOver={(event) => {
                    isActive && setOver(true);
                }}
                onMouseOut={(event) => {
                    isActive && setOver(false);
                }}
                onClick={() => {
                    isActive && onClick();
                }}
            >
                <rect
                    className={clsx(
                        defaultClasses.buttonOverShape,
                        classes.buttonOverShape
                    )}
                    x ='0'
                    y ='0'
                    height = {iconSize}
                    width = {iconSize}
                />
                <path
                    className={clsx(
                        defaultClasses.buttonIcon,
                        classes.buttonIcon,
                        isOver && defaultClasses.buttonIconOver,
                        isOver && classes.buttonIconOver
                    )}
                    d={path}
                />
            </g>
        </ToolTip>
    );
}

Button.propTypes = {
    tipPlacement: PropTypes.string.isRequired,
    tipOverlay: PropTypes.string.isRequired,
    path: PropTypes.string.isRequired,
    origin: PropTypes.shape({
        x: PropTypes.number.isRequired,
        y: PropTypes.number.isRequired
    }).isRequired,
    scale: PropTypes.number.isRequired,
    iconSize: PropTypes.number.isRequired,
    onClick: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired,
    rcToolTipPrefixCls: PropTypes.string.isRequired,
    isActive: PropTypes.bool.isRequired,
};
