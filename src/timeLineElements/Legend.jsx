import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import theme from '../theme';
import {makeStyles} from '@material-ui/styles';

const useStyles = makeStyles({
    legend: {
        fontSize: theme.font.legendSize,
        lineHeight: '125%',
        fontFamily: theme.font.family,
        fillOpacity: 1,
        stroke:'none',
        textAlign:'end',
        textAnchor:'end',
        userSelect: 'none'
    }
});

export default function Legend({classes, metricsDefinition, origin}){

    const fill = [...metricsDefinition.colors].reverse();
    const legends = [...metricsDefinition.legends].reverse();

    const defaultClasses = useStyles();
    const className = (className) => clsx(defaultClasses[className], classes[className]);

    return (
        <g
            transform={`translate(${origin.x},${origin.y})`}
        >
            {legends.map((leg,i) => (
                <text
                    key={i}
                    className={className('legend')}
                    style={{
                        fill: fill[i].text
                    }}
                    x={0}
                    y={14*(i+1)}
                >
                    <tspan>{leg}</tspan>
                </text>
            ))}
        </g>
    )
}

Legend.propTypes = {
    classes: PropTypes.object.isRequired,
    origin: PropTypes.shape({
        x: PropTypes.number.isRequired,
        y: PropTypes.number.isRequired
    }).isRequired,
    metricsDefinition: PropTypes.shape({
        count: PropTypes.number.isRequired,
        legends: PropTypes.arrayOf(PropTypes.string).isRequired,
        colors: PropTypes.arrayOf(PropTypes.shape({
            fill: PropTypes.string.isRequired,
            stroke: PropTypes.string.isRequired,
            text: PropTypes.string.isRequired,
        })).isRequired
    }).isRequired,
};
