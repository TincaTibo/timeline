import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import theme from '../theme';
import {makeStyles} from '@material-ui/styles';
import moment from 'moment-timezone';
import ToolTip from '../ToolTip';

const qualityHeight = 3;

const useStyles = makeStyles({
    qualityCell: {
        strokeWidth: 0,
    }
});

export default function QualityLine({classes, rcToolTipPrefixCls, origin, quality, xAxis, qualityScale}){

    const defaultClasses = useStyles();
    const className = (className) => clsx(defaultClasses[className], classes[className]);

    return (
        <g transform={`translate(${origin.x}, ${origin.y})`}>
            {
                quality.items.map((item, i) => {
                    const x1 = xAxis(item.time);
                    const x2 = xAxis(moment(item.time).add(quality.intervalMin, 'minutes'));
                    const rect = (
                        <rect
                            key={i}
                            className={className('qualityCell')}
                            style={{
                                fill: qualityScale(item.quality),
                            }}
                            onMouseOver={(event) => { event.target.style.fill = theme.colors.qualityFillSelected; }}
                            onMouseOut={(event) => { event.target.style.fill = qualityScale(item.quality); }}
                            x={x1}
                            y={1}
                            width={x2-x1}
                            height={qualityHeight}
                        />
                    );

                    if(item.tip){
                        return (
                            <ToolTip
                                key={i}
                                placement={'bottom'}
                                overlay={item.tip}
                                mouseEnterDelay={0.1}
                            >
                                {rect}
                            </ToolTip>
                        )
                    }
                    else return rect;
                })
            }
        </g>
    );
}

QualityLine.propTypes = {
    classes: PropTypes.object.isRequired,
    rcToolTipPrefixCls: PropTypes.string.isRequired,
    origin: PropTypes.shape({
        x: PropTypes.number.isRequired,
        y: PropTypes.number.isRequired
    }).isRequired,
    xAxis: PropTypes.func.isRequired,
    quality: PropTypes.shape({
        items: PropTypes.arrayOf(PropTypes.shape({
            time: PropTypes.instanceOf(moment).isRequired,
            quality: PropTypes.number.isRequired,
            tip: PropTypes.node
        })),
        intervalMin: PropTypes.number
    }).isRequired,
    qualityScale: PropTypes.func.isRequired,
};
