import {makeStyles} from '@material-ui/styles';
import theme from '../theme';

export const arrow = `m 0,-5 5,5 -5,5`;

export const useStyles = makeStyles({
    axis: {
        fill: 'none',
        stroke: theme.colors.medium,
        strokeWidth: 1,
    },
    now: {
        fill: theme.colors.mediumDark,
        stroke: theme.colors.mediumDark,
        strokeWidth: 1,
    },
    arrow: {
        fill: theme.colors.medium,
        fillOpacity: 1,
        stroke: theme.colors.medium,
        strokeWidth: 1,
    },
    grid: {
        stroke: theme.colors.light,
        strokeWidth: 1,
    },
    limitMarker: {
        stroke: theme.colors.medium,
        strokeWidth: 1,
    },
    timeAxisText: {
        fontSize: theme.font.axisTextSize,
        fontFamily: theme.font.family,
        fill: theme.colors.medium,
        textAlign: 'center',
        textAnchor: 'middle',
        userSelect: 'none'
    },
    timeAxisDaysText: {
        fill: theme.colors.mediumDark,
    },
    verticalAxisText: {
        fontSize: theme.font.axisTextSize,
        fontFamily: theme.font.family,
        fill: theme.font.family,
        textAlign: 'end',
        textAnchor: 'end',
        userSelect: 'none'
    }
});
