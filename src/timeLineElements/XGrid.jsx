import React from 'react';
import moment from 'moment-timezone';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import {useStyles, arrow} from './axesStyles';

export default function XAxis({origin, marks, xAxis, classes, yAxisHeight}){

    const defaultClasses = useStyles();

    const className = (className) => clsx(defaultClasses[className], classes[className]);

    return (
        <g transform={`translate(${origin.x}, ${origin.y})`}>
            {/* Grid */}
            {
                marks && marks.map((m, index) => (
                        <g
                            key={m.toISOString()}
                            transform={`translate(${xAxis(m)},0)`}
                        >
                            <path
                                className={className('grid')}
                                d={`M 0,0 0,${-yAxisHeight}`}
                            />
                        </g>
                    )
                )
            }
        </g>
    );
}

XAxis.propTypes = {
    classes: PropTypes.object,
    origin: PropTypes.shape({
        x: PropTypes.number.isRequired,
        y: PropTypes.number.isRequired
    }).isRequired,
    marks: PropTypes.array,
    xAxis: PropTypes.func,
    yAxisHeight: PropTypes.number,
};

XAxis.defaultProps = {
    arrowPath: arrow,
}
