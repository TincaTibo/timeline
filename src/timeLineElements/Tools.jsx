import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment-timezone';
import Button from './Button';

const doubleArrowIcon = 'm 0,0 5,5 0,-5 5,5 -5,5 0,-5 -5,5 Z';
const resetIcon = 'M5 15H3v4c0 1.1.9 2 2 2h4v-2H5v-4zM5 5h4V3H5c-1.1 0-2 .9-2 2v4h2V5zm14-2h-4v2h4v4h2V5c0-1.1-.9-2-2-2zm0 16h-4v2h4c1.1 0 2-.9 2-2v-4h-2v4zM12 9c-1.66 0-3 1.34-3 3s1.34 3 3 3 3-1.34 3-3-1.34-3-3-3z';
const gotoNowIcon = 'M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8zm.5-13H11v6l5.25 3.15.75-1.23-4.5-2.67z';

export default function Tools({   tools, classes, rcToolTipPrefixCls,
                                  labels, histoWidth, isActive, origin,
                                  onResetTime, shiftTimeLine, onGoto
}){

    return (
        <g transform={`translate(${origin.x}, ${origin.y})`}
        >
            {tools.slideForward!==false &&
            <Button
                classes={classes}
                rcToolTipPrefixCls={rcToolTipPrefixCls}
                tipPlacement="left"
                tipOverlay={labels.forwardButtonTip}
                origin={{x: histoWidth + 22, y: -5}}
                scale={1}
                path={doubleArrowIcon}
                iconSize={10}
                isActive={isActive}
                onClick={() => {shiftTimeLine(-120)}}
            />
            }

            {tools.slideBackward!==false &&
            <Button
                classes={classes}
                rcToolTipPrefixCls={rcToolTipPrefixCls}
                tipPlacement="right"
                tipOverlay={labels.backwardButtonTip}
                origin={{x: -7, y: 5}}
                scale={-1}
                path={doubleArrowIcon}
                iconSize={10}
                isActive={isActive}
                onClick={() => {shiftTimeLine(120)}}
            />
            }

            {tools.resetTimeline!==false &&
            <Button
                classes={classes}
                rcToolTipPrefixCls={rcToolTipPrefixCls}
                tipPlacement="left"
                tipOverlay={labels.resetButtonTip}
                origin={{x: histoWidth + 17, y: -30}}
                scale={0.75}
                path={resetIcon}
                iconSize={24}
                isActive={isActive}
                onClick={onResetTime}
            />
            }

            {tools.gotoNow!==false &&
            <Button
                classes={classes}
                rcToolTipPrefixCls={rcToolTipPrefixCls}
                tipPlacement="left"
                tipOverlay={labels.gotoNowButtonTip}
                origin={{x: histoWidth + 17, y: tools.resetTimeline!==false ? -53 : -30}}
                scale={0.75}
                path={gotoNowIcon}
                iconSize={24}
                isActive={isActive}
                onClick={onGoto}
            />
            }
        </g>
    );
}

Tools.propTypes = {
    tools: PropTypes.shape({
        slideForward: PropTypes.bool,
        slideBackward: PropTypes.bool,
        resetTimeline: PropTypes.bool,
        gotoNow: PropTypes.bool,
        cursor: PropTypes.bool,
    }).isRequired,
    classes: PropTypes.object.isRequired,
    rcToolTipPrefixCls: PropTypes.string.isRequired,
    labels: PropTypes.shape({
        forwardButtonTip: PropTypes.string.isRequired,
        backwardButtonTip: PropTypes.string.isRequired,
        resetButtonTip: PropTypes.string.isRequired,
        gotoNowButtonTip: PropTypes.string.isRequired,
    }).isRequired,
    histoWidth: PropTypes.number.isRequired,
    isActive: PropTypes.bool.isRequired,
    origin: PropTypes.shape({
        x: PropTypes.number.isRequired,
        y: PropTypes.number.isRequired
    }).isRequired,
    onResetTime: PropTypes.func.isRequired,
    shiftTimeLine: PropTypes.func.isRequired,
    onGoto: PropTypes.func.isRequired
};
