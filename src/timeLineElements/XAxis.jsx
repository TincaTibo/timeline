import React from 'react';
import moment from 'moment-timezone';
import PropTypes from 'prop-types';
import {timeDay} from 'd3-time';
import clsx from 'clsx';

import {useStyles, arrow} from './axesStyles';

export default function XAxis({min, max, origin, axisWidth, marks, xAxis, classes, arrowPath, onFormatTimeLegend}){

    const now = moment();
    const defaultClasses = useStyles();

    const className = (className) => clsx(defaultClasses[className], classes[className]);

    return (
        <g transform={`translate(${origin.x}, ${origin.y})`}
        >
            {/* Horizontal Axis */}
            <path
                className={className('axis')}
                d={`M 0,-6 0,6`}
            />
            <path
                className={className('axis')}
                d={`M 0,0 ${axisWidth + 5},0`}
            />
            <g transform={`translate(${axisWidth},0)`}>
                <path
                    className={className('arrow')}
                    d={arrowPath}
                />
            </g>

            {/* Marks */}
            {
                marks && marks.map((m, index) => (
                        <g
                            key={m.toISOString()}
                            transform={`translate(${xAxis(m)},0)`}
                        >
                            <path
                                className={className('limitMarker')}
                                d='M 0,0 0,6'
                            />
                            <text x={0}
                                  y={15}
                                  className={clsx(className('timeAxisText'), timeDay(m) >= m && className('timeAxisDaysText'))}
                            >
                                {onFormatTimeLegend(m)}
                            </text>
                        </g>
                    )
                )
            }

            {/* Now */}
            { xAxis && now.isSameOrBefore(max) && now.isSameOrAfter(min) &&
            <path
                transform={`translate(${xAxis(now)},0)`}
                className={className('now')}
                d={`m -3,6 6,0 -3,-6 Z`}
            />
            }
        </g>
    );
}

XAxis.propTypes = {
    axisWidth:PropTypes.number.isRequired,
    classes: PropTypes.object,
    min: PropTypes.instanceOf(moment),
    max: PropTypes.instanceOf(moment),
    origin: PropTypes.shape({
        x: PropTypes.number.isRequired,
        y: PropTypes.number.isRequired
    }).isRequired,
    marks: PropTypes.array,
    xAxis: PropTypes.func,
    arrowPath: PropTypes.string,
    onFormatTimeLegend: PropTypes.func.isRequired,
};

XAxis.defaultProps = {
    arrowPath: arrow,
}
