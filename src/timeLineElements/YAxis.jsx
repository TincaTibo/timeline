import React from 'react';
import PropTypes from 'prop-types';

import {useStyles, arrow} from './axesStyles';
import clsx from 'clsx';

export default function YAxis({classes, marks, yAxis, onFormatMetricLegend, maxHeight, origin, arrowPath}){

    const defaultClasses = useStyles();
    const className = (className) => clsx(defaultClasses[className], classes[className]);

    return (
        <g transform={`translate(${origin.x}, ${origin.y})`}
        >
            <path
                className={className('axis')}
                d={`M 0,0 0, ${-maxHeight - 5}`}
            />
            <g transform={`translate(0, ${-maxHeight}) rotate(270)`}>
                <path
                    className={className('arrow')}
                    d={arrowPath}
                />
            </g>

            {
                marks.map((m, index) => (
                    <g
                        key={index}
                        transform={`translate(0,${-yAxis(m)})`}
                    >
                        {m > 0 &&
                        <text
                            transform={`translate(0,5)`}
                            className={className('verticalAxisText')}
                            x={-7}
                            y={-3}
                        >
                            {onFormatMetricLegend(m)}
                        </text>
                        }
                    </g>
                ))
            }
        </g>
    );
}

YAxis.propTypes = {
    classes: PropTypes.object,
    maxHeight:PropTypes.number.isRequired,
    origin: PropTypes.shape({
        x: PropTypes.number.isRequired,
        y: PropTypes.number.isRequired
    }).isRequired,
    marks: PropTypes.array,
    yAxis: PropTypes.func,
    arrowPath: PropTypes.string,
    onFormatMetricLegend: PropTypes.func.isRequired,
};

YAxis.defaultProps = {
    arrowPath: arrow,
}
