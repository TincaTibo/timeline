import React from 'react';
import moment from 'moment-timezone';
import _sum from 'lodash-es/sum';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {makeStyles} from '@material-ui/styles';
import ToolTip from '../ToolTip';

const useStyles = makeStyles({
    histoItem: {
        fillOpacity: 1,
        strokeWidth: 1,
    },
    histoHovered: {
        strokeWidth: 0,
        fill: '#000000',
        fillOpacity: 0.3
    },
});

export default function Histogram({
  classes, items, metricsDefinition, origin,
  barHovered, tooltipVisible, verticalScale, dragging,
  HistoToolTip, onFormatMetricLegend, onFormatTimeToolTips
}){

    if(!items){
        return null;
    }

    const defaultClasses= useStyles();
    const className = (className) => clsx(defaultClasses[className], classes[className]);

    return <g transform={`translate(${origin.x}, ${origin.y})`}>
        {
            items.map((item, i) => (
                <g key={i}>
                    {
                        metricsDefinition.colors.map((color, m) => (
                            item.metrics[m] > 0 &&
                            <rect
                                key={m}
                                className={className('histoItem')}
                                style={{
                                    fill: color.fill,
                                    stroke: color.stroke,
                                }}
                                x={item.x1}
                                y={-verticalScale(_sum(item.metrics.slice(0, m + 1)))}
                                width={item.x2-item.x1}
                                height={verticalScale(item.metrics[m])}
                            />
                        ))
                    }
                    {
                        barHovered === i && !dragging &&
                        <ToolTip
                            overlay={
                                <HistoToolTip
                                    item={item}
                                    metricsDefinition={metricsDefinition}
                                    onFormatTimeToolTips={onFormatTimeToolTips}
                                    onFormatMetricLegend={onFormatMetricLegend}
                                    classes={classes}
                                />
                            }
                            visible={tooltipVisible}
                        >
                            <rect
                                className={className('histoHovered')}
                                x={item.x1}
                                y={-item.height}
                                width={item.x2-item.x1}
                                height={item.height}
                            />
                        </ToolTip>
                    }
                </g>
            ))
        }
    </g>;
}

Histogram.propTypes = {
    classes: PropTypes.object,
    items: PropTypes.arrayOf(PropTypes.shape({
        start: PropTypes.instanceOf(moment).isRequired,
        end: PropTypes.instanceOf(moment).isRequired,
        x1: PropTypes.number,
        x2: PropTypes.number,
        metrics: PropTypes.arrayOf(PropTypes.number).isRequired,
        total: PropTypes.number.isRequired,
    })),
    metricsDefinition: PropTypes.shape({
        count: PropTypes.number.isRequired,
        legends: PropTypes.arrayOf(PropTypes.string).isRequired,
        colors: PropTypes.arrayOf(PropTypes.shape({
            fill: PropTypes.string.isRequired,
            stroke: PropTypes.string.isRequired,
            text: PropTypes.string.isRequired,
        })).isRequired
    }).isRequired,
    origin: PropTypes.shape({
        x: PropTypes.number.isRequired,
        y: PropTypes.number.isRequired
    }).isRequired,
    verticalScale: PropTypes.func.isRequired,
    onFormatTimeToolTips: PropTypes.func.isRequired,
    onFormatMetricLegend: PropTypes.func.isRequired,
    HistoToolTip: PropTypes.elementType,
    barHovered: PropTypes.number,
    tooltipVisible: PropTypes.bool,
};
