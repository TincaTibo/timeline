import React from 'react';
import PropTypes from 'prop-types';

import {useStyles, arrow} from './axesStyles';
import clsx from 'clsx';

export default function YAxis({classes, marks, yAxis, origin, xAxisWidth}){

    const defaultClasses = useStyles();
    const className = (className) => clsx(defaultClasses[className], classes[className]);

    return (
        <g transform={`translate(${origin.x}, ${origin.y})`}
        >
            {
                marks.map((m, index) => (
                    <g
                        key={index}
                        transform={`translate(0,${-yAxis(m)})`}
                    >
                        <path
                            className={className('grid')}
                            d={`M -5,0 ${xAxisWidth},0`}
                        />
                    </g>
                ))
            }
        </g>
    );
}

YAxis.propTypes = {
    classes: PropTypes.object,
    origin: PropTypes.shape({
        x: PropTypes.number.isRequired,
        y: PropTypes.number.isRequired
    }).isRequired,
    marks: PropTypes.array,
    yAxis: PropTypes.func,
    xAxisWidth: PropTypes.number,
};

YAxis.defaultProps = {
    arrowPath: arrow,
}
