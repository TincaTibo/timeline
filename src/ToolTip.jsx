import React from 'react';
import ToolTipBase from 'rc-tooltip';
import PropTypes from 'prop-types';

import './tipDark.css';
import theme from './theme';

export default function ToolTip({overlay, placement, mouseEnterDelay, prefixCls, align, visible, hidden, children}){
    const extraProps = {};
    if(visible) extraProps.visible = true;
    if(hidden) extraProps.visible = false;

    return (
        <ToolTipBase
            placement={placement}
            prefixCls={prefixCls || theme.rcToolTipPrefixCls}
            overlay={overlay}
            mouseEnterDelay={mouseEnterDelay}
            destroyTooltipOnHide={true}
            align={align}
            {...extraProps}
        >
            {children}
        </ToolTipBase>
    );
}

ToolTip.propTypes = {
    overlay: PropTypes.any.isRequired,
    placement: PropTypes.string,
    mouseEnterDelay: PropTypes.number,
}

ToolTip.defaultProps = {
    placement: 'top',
    mouseEnterDelay: 0.5,
}
