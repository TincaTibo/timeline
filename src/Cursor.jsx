import React, {useRef, useState} from 'react';
import PropTypes from 'prop-types';

import {hide, show} from './cursorElements/utils';

import DragOverlay from './cursorElements/DragOverlay';
import ZoomOut from './cursorElements/ZoomOut';
import CursorIcon from './cursorElements/CursorIcon';
import CursorSelection from './cursorElements/CursorSelection';
import LeftHandle from './cursorElements/LeftHandle';
import RightHandle from './cursorElements/RightHandle';
import ZoomIn from './cursorElements/ZoomIn';
import LeftToolTip from './cursorElements/LeftToolTip';
import RightToolTip from './cursorElements/RightToolTip';
import moment from 'moment-timezone';

export default function Cursor({
        overlayWidth, overlayHeight,
        items, setBarHovered, setToolTipVisible, showHistoToolTip, tooltipVisible, barHovered,
        dragging, setDragging,
        origin, height,
        startPos, endPos,
        canZoom, minZoom, maxZoom, maxSize,
        zoomOutLabel, zoomOut,
        cursorIsAfterView, cursorIsBeforeView,
        gotoCursorLabel, onGotoCursor,
        rcToolTipPrefixCls, classes,
        onStartDrawCursor, onDrawCursor, onEndCursor,
        onMoveDomain, onMovedDomain, onDragCursor, onEndDragCursor,
        startText, stopText,
        zoomInLabel, zoomIn,
        startIsOutOfView, endIsOutOfView, tools,
        onResizeLeftCursor, onResizeRightCursor, onEndResizeCursor
    }
){
    const dragOverlay = useRef();
    const zoomInButton = useRef();
    const cursorSelection = useRef();

    const renderIcon = (cursorIsAfterView || cursorIsBeforeView);
    const cursorWidth = endPos - startPos < 1 ? 1 : endPos - startPos;

    const [isVisibleTTR, setVisibleTTR] = useState(false);
    const [isVisibleTTL, setVisibleTTL] = useState(false);

    const originSelection = {
        x: startPos,
        y: 0
    };

    return (
        <g transform={`translate(${origin.x},${origin.y})`}>
            <DragOverlay
                ref={dragOverlay}
                width={overlayWidth}
                height={overlayHeight}
                showToolTipLeft={setVisibleTTL}
                showToolTipRight={setVisibleTTR}
                onDrawCursor={onDrawCursor}
                onEndCursor={onEndCursor}
                onMovedDomain={onMovedDomain}
                onMoveDomain={onMoveDomain}
                onStartDrawCursor={onStartDrawCursor}

                setToolTipVisible={setToolTipVisible}
                setBarHovered={setBarHovered}
                items={items}
                tooltipVisible={tooltipVisible}
                barHovered={barHovered}
                showHistoToolTip={showHistoToolTip}
                marginBottom={overlayHeight - height - 4}
                dragging={dragging}
                setDragging={setDragging}
                cursorSelection={cursorSelection}
            />
            { renderIcon &&
                <CursorIcon
                    classes={classes}
                    rcToolTipPrefixCls={rcToolTipPrefixCls}
                    origin={{
                        x: cursorIsAfterView ? startPos-17 : startPos-23,
                        y: height/2-24
                    }}
                    position={startPos === 1 ? 'left' : 'right'}
                    gotoCursorLabel={gotoCursorLabel}
                    onGotoCursor={onGotoCursor}
                />
            }
            <g style={{display: renderIcon ? 'none' : undefined}}
               transform={`translate(${originSelection.x},${originSelection.y})`}
               onMouseOver={() => {canZoom && tools.zoomIn!==false && !maxZoom && show(zoomInButton.current)}}
               onMouseOut={() => {canZoom && tools.zoomIn!==false && hide(zoomInButton.current)}}
            >
                <CursorSelection
                    ref={cursorSelection}
                    classes={classes}
                    dragOverlay={dragOverlay}
                    maxSize={maxSize}
                    canZoom={canZoom}
                    startPos={startPos}
                    endPos={endPos}
                    height={height}
                    showToolTipLeft={setVisibleTTL}
                    showToolTipRight={setVisibleTTR}
                    onDragCursor={onDragCursor}
                    onEndDragCursor={onEndDragCursor}
                    onMovedDomain={onMovedDomain}
                    onMoveDomain={onMoveDomain}
                    zoomIn={zoomIn}
                    width={cursorWidth}

                    setToolTipVisible={setToolTipVisible}
                    setBarHovered={setBarHovered}
                    items={items}
                    tooltipVisible={tooltipVisible}
                    barHovered={barHovered}
                    showHistoToolTip={showHistoToolTip}
                    dragging={dragging}
                    setDragging={setDragging}
                />
                <LeftHandle
                    classes={classes}
                    origin={{
                        x: 0,
                        y: height/2+2
                    }}
                    maxSize={maxSize}
                    dragOverlay={dragOverlay}
                    startIsOutOfView={startIsOutOfView}
                    showToolTipLeft={setVisibleTTL}
                    onResizeLeftCursor={onResizeLeftCursor}
                    onEndResizeCursor={onEndResizeCursor}
                    setDragging={setDragging}
                />
                <RightHandle
                    classes={classes}
                    origin={{
                        x: cursorWidth,
                        y: height/2+2
                    }}
                    maxSize={maxSize}
                    dragOverlay={dragOverlay}
                    endIsOutOfView={endIsOutOfView}
                    showToolTipRight={setVisibleTTR}
                    onResizeRightCursor={onResizeRightCursor}
                    onEndResizeCursor={onEndResizeCursor}
                    setDragging={setDragging}
                />
                {canZoom && tools.zoomIn!==false &&
                <ZoomIn
                    ref={zoomInButton}
                    classes={classes}
                    rcToolTipPrefixCls={rcToolTipPrefixCls}
                    origin={{
                        x: cursorWidth - 25,
                        y: 0
                    }}
                    zoomInLabel={zoomInLabel}
                    onZoomIn={zoomIn}
                />}
                <LeftToolTip
                    classes={classes}
                    origin={{
                        x: 0,
                        y: height/2
                    }}
                    height={height}
                    startText={startText}
                    isVisibleTooltipLeft={isVisibleTTL}
                />
                <RightToolTip
                    classes={classes}
                    origin={{
                        x: cursorWidth,
                        y: height/2 + 4
                    }}
                    height={height}
                    stopText={stopText}
                    isVisibleTooltipRight={isVisibleTTR}
                />
            </g>
            { canZoom && !minZoom && tools.zoomOut!==false &&
            <ZoomOut
                classes={classes}
                rcToolTipPrefixCls={rcToolTipPrefixCls}
                origin={{
                    x: overlayWidth - 10,
                    y: height - 20
                }}
                zoomOutLabel={zoomOutLabel}
                onZoomOut={zoomOut}
            />}
        </g>
    );
}

Cursor.propTypes = {
    origin: PropTypes.shape({
        x: PropTypes.number.isRequired,
        y: PropTypes.number.isRequired
    }).isRequired,

    startPos: PropTypes.number.isRequired,
    startIsOutOfView: PropTypes.bool,
    endPos: PropTypes.number.isRequired,
    endIsOutOfView: PropTypes.bool,
    height: PropTypes.number.isRequired,

    overlayHeight: PropTypes.number.isRequired,
    overlayWidth: PropTypes.number.isRequired,
    items: PropTypes.arrayOf(PropTypes.shape({
        start: PropTypes.instanceOf(moment).isRequired,
        end: PropTypes.instanceOf(moment).isRequired,
        x1: PropTypes.number,
        x2: PropTypes.number,
        metrics: PropTypes.arrayOf(PropTypes.number).isRequired,
        total: PropTypes.number.isRequired,
    })),
    tooltipVisible: PropTypes.bool,
    barHovered: PropTypes.number,
    dragging: PropTypes.bool,
    setBarHovered: PropTypes.func,
    setToolTipVisible: PropTypes.func,
    setDragging: PropTypes.func,

    cursorIsBeforeView: PropTypes.bool,
    cursorIsAfterView: PropTypes.bool,

    maxSize: PropTypes.bool,

    startText: PropTypes.string.isRequired,
    stopText: PropTypes.string.isRequired,

    canZoom: PropTypes.bool.isRequired,
    maxZoom: PropTypes.bool,
    minZoom: PropTypes.bool,

    zoomIn: PropTypes.func,
    zoomOut: PropTypes.func,

    onResizeLeftCursor: PropTypes.func.isRequired,
    onResizeRightCursor: PropTypes.func.isRequired,
    onEndResizeCursor: PropTypes.func.isRequired,

    onDragCursor: PropTypes.func.isRequired,
    onEndDragCursor: PropTypes.func.isRequired,

    onStartDrawCursor: PropTypes.func.isRequired,
    onDrawCursor: PropTypes.func.isRequired,
    onEndCursor: PropTypes.func.isRequired,

    onMoveDomain: PropTypes.func,
    onMovedDomain: PropTypes.func,

    onGotoCursor: PropTypes.func,
    gotoCursorLabel: PropTypes.string,
    zoomInLabel: PropTypes.string,
    zoomOutLabel: PropTypes.string,

    classes: PropTypes.object,
    rcToolTipPrefixCls: PropTypes.string.isRequired,

    tools: PropTypes.shape({
        zoomIn: PropTypes.bool,
        zoomOut: PropTypes.bool,
    }),
};

const identity = () => {};

Cursor.defaultProps = {
    classes: {},
    onMoveDomain: identity,
    onMovedDomain: identity,
    setBarHovered: identity,
    setToolTipVisible: identity,
    setDragging: identity,
}
