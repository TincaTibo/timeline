import React, { useState, useEffect, useRef, forwardRef } from 'react';
import { makeStyles } from '@material-ui/styles';

import TimeLine from './TimeLine';

const defaultWidth = 900;
const defaultHeight = 100;

const useStyles = makeStyles((theme) => ({
    timelineContainer: {
        width: '100%', height: '100%'
    }
}));

function TimeLineResizer(props){

    const [dimensions, setDimensions] = useState({
        width: defaultWidth,
        height: defaultHeight,
    });

    const container = useRef(null);

    const onViewResize = () => {
        if(container.current){
            setDimensions({
                width: container.current.clientWidth,
                height: container.current.clientHeight
            });
        }
    };

    useEffect(() => {
        if(container.current){
            onViewResize();
            window.addEventListener('resize', onViewResize);
        }
    }, [container.current]);

    const classes = useStyles();

    return  (
        <div
            ref={container}
            className={classes.timelineContainer}
        >
            <TimeLine
                ref={props.forwardedRef}
                width={dimensions.width}
                height={dimensions.height}
                {...props}
            />
        </div>
    )
}


function timeLineFwd(props, ref) {
    return <TimeLineResizer {...props} forwardedRef={ref}/>
}

export default forwardRef(timeLineFwd);
