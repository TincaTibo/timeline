##4.0.0
- Changed to moment-timezone to manage change of default timezone in parent application

## 3.4.0
- Dependencies upgrade (test app included)
- Still in React 16 as some deps are still dependant of it

## 3.3.3
- Typo & refactor

## 3.3.2
- Fix: Provide default function in Cursor element for setTooltipVisible, setBarHovered and setDragging so that it
  works on standalone (without hovering feature) -> range filters on Spider.

## 3.3.1
- Fix: do not display bar being hovered and tooltip will drawing, dragging or resizing cursor

## 3.3.0
- New prop `selectBarOnClick` to automatically select the time of a histogram bar when clicking on it

## 3.2.0
- Histogram Toolips improvements
    - Refactor of histo tooltips functions
    - Better handling transition phase between dragoverlay and cursorSelection
      -> no timer restart to display tooltip
      -> using cross forward refs and related target of the event to know where the mouse is going
      -> kindda crazy, but does the job :)
    - When moving outside bar and bar, timer is triggered to display tooltip
    - When extending cursor, bars are not shown hovered (I forgot this one ;) )

## 3.1.0
- Allow showing tooltips with metrics when hovering the histogram bars: `showHistoToolTip` prop
- Allow overriding the tooltip content: `HistoToolTip` prop

## 3.0.0
- Allow overriding default zoom factor for zoom out introduced in 2.2.0: `zoomFactor` prop
- Remove widening/shortening domain when zooming out at start
- Remove widening/shortening domain when sliding is blocked by max domain
- Sliding the timeline is stopped when reaching min/max domain
- Removed tooltips from sliding buttons that could be of two values: extend/slide. Now they only slide. /!\ Interface breach!
- Added new message events when timeline reaches max/min accepted domain (min/max time)
- Fix not adding domain in domains when zooming 

## 2.4.0
- Truncate start of domain and not end went zooming out / providing a too big domain
- Make sure of immutability of provided domains

## 2.3.0
- Moved the grid lines in X & Y to the back of the histogram

## 2.2.2
- Changes in package.json dependencies not to have our own version of Material UI styling lib -> causes clashes if main app has another version

## 2.2.1
- Fixes in zoom out - take into account maxDomain

## 2.2.0
- Add possibility to zoom out from start, indefinitely unless biggestVisibleDomain is set 

## 2.1.0
- Add timeAxisDaysText className for timeAxis label for days, start of month and start of year

## 2.0.7
- Fix on quality line hover: fill must be changed by javascript mouse events as it is an inline style

## 2.0.6
- Added check to avoid error on console when timeline disappears after resizing

## 2.0.5
- Fix typo in prop for cursorIcon action that prevented going back to selection

## 2.0.4
- Fixed zoomIn and zoomOut tools deactivation test for better usage of API: active when nothing is specified for them, but tools options are specified for others

## 2.0.3
- ZoomIn and ZoomOut tools default to true

## 2.0.2
- Fixed dependencies version for more compatibilities (but if many d3-selection loaded at once)
- Increased default tooltip scale

## 2.0.1
- Fixed resize on changed width
- Fixed README

## 2.0.0
- Split into subcomponents for better maintenance. Much better code structure.
- Bring classes prop to customise any style
- Removed style prop
- Added customizing ticks and showGrid option for axes
- Added customizing margins
- Fixed cursor tooltip resizing
- Allowed not displaying zoom icons
- Allowed not displaying legend
- Allowed changing arrow path
- Exposed functions to zoom and slide into ref of component

## 1.3.8
- Technical upgrade: MUI styling solution, libs upgrade
- Fixes on demo

## 1.3.7
- Fix in domain change comparison on update: check moment difference, not object

## 1.3.6
- Live example link on Readme

## 1.3.5
- Fixed change of default behavior in d3.drag that prevented using Ctrl for dragging: https://github.com/d3/d3-drag/issues/62

## 1.3.4
- All quality slots are displayed. Filtering (not to show === 1 for instance) should be done on caller side.
- Domain is thus not any more limited to [0-1], could be any (matching the scale).

## 1.3.3
- Quality lines tips can be react nodes

## 1.3.2
- Fixed display issues when showing only part of a quality line slots
- Fixed display of last items of metrics histo

## 1.3.1
- Missed a React warning...

## 1.3.0
- Added quality line

## 1.2.10
- Updated dependencies for security following npm audit

## 1.2.9
- Fixed the bug that caused selected time to be x2 when clicking on Goto Now icon

## 1.2.8
- Removed a forgotten console.log

## 1.2.7
- Fix the bug that when maxZoom was reached, you were blocked in this state if you:
  * Reset the timeline
  * Resized the timeline

## 1.2.6
- Removed some unused code
- Fixed test on start and stop props update: it was done on Moment object change and not on Moment date value (use of isSame)

## 1.2.5
Visual improvements
- Move zoom icon further to the right
- Move left cursor icon when cursor is before view further to the left
- Removed white background (specific for Spider)

## 1.2.4
- Bug fix to take into account smallestResolution as minimum interval when calling aggregation API (was 1 min)
- Removed concatenation of smallestResolution.humanize() in labels.zoomSelectionResolutionExtended.
  * Was not working well.
  * New default: "You reached maximum zoom level"

## 1.2.3
- Many fixes after inclusion back to Spider
  - Load items on mount if domain is defined
  - Change reference for mouse events so that TimeLine can be included without TimeLineResizer
  - Visual fixes
  - Better handling of changing width when no items are loaded
  - Better handling of tools props
  - ...

## 1.2.2
- Fix: reattach event handlers after moving from cursor icon to cursor
- Fix: better handling on move to cursor => center domain on it (if possible)

## 1.2
- Add action on cursor icon to goto cursor
- Add tooltip for cursor icon
- Add tooltips for zoom in/out icons
- Remove componentWillReceiveProps (deprecated)

## 1.1
- Improve legend positioning
- Upgrade technical stack (React, D3, moment)
- Upgrade demo server stack (Koa, Webpack, Babel...)
- Improve deps for size + example of webpack optim (demo)
- maxDomain.min|max are both optional
- When on max zoom level with scrolling, display message and avoid jump (changing domain)
- Adapt left margin to legend 
- Move tools more to the right
- New prop to set what tools are active
- New prop to set max selectable duration.
    * Cursor and mouse pointer changes when max is reached.
    * A new message is displayed.
- Added showMessage toasts in demo
- Added autosliding when dragging with cursor outside chart area
- Added possibility to fetch data while sliding domain
- When selection cursor is outside view, show a small icon rather than the cursor 

## 1.0.5
- Fix index.js by removing jsx extension in require

## 1.0.4
- Fix package.json main point

## 1.0.1, 1.0.2, 1.0.3
- Fixes to limit module size on npm

## 1.0
- Transpile to ES5
- Add webpack config for demo
- Migrated to React 16
- Published to NPM
- Ability to limit max duration

## 0.1
- Extracted from Spider-GUI
- Removed dependency on Material UI (removed menu)
- Removed dependency on co
- Build sample without redux & saga
- Cleanup API: simplification & refactor
- Improved code quality & speed
- Added auto resize
- Added goto now feature
- Moved structure to NPM module with test app
- Flexible smallestDuration limit
- Better proptypes
- Label extraction for i18n
- Documentation (toc: markdown-toc --no-firsth1 README.md)
